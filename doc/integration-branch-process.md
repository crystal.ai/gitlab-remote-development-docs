# Integration Branch Process

[[_TOC_]]

## Goal

Allow the Remote Development team to [collaborate](https://about.gitlab.com/handbook/values/#collaboration) and [iterate](https://about.gitlab.com/handbook/values/#iteration) quickly and [efficiently](https://about.gitlab.com/handbook/values/#efficiency) in the early stages of developing the Remote Development feature, and achieve our desired [result](https://about.gitlab.com/handbook/values/#results) of delivering a functional MVC/[experiment](https://docs.gitlab.com/ee/policy/alpha-beta-support.html#experiment) with a small team in a short timeframe.

## Overview

Remote Development is a greenfield feature with many unknowns, and we knew when we started that it would surely involve much exploration and changes in direction, and also coordination of new features across multiple repos and projects.

Therefore, the team decided to use an _integration branch_ pattern, with an integration branch named "`remote_dev`" for every existing project/repo which required changes.

This is very similar to the [Team Integration Branch pattern](https://martinfowler.com/articles/branching-patterns.html#team-integration-branch) described by Martin Fowler, but with some nuances specific to our needs.

See the following issue for more context on this plan and the relevant branches: https://gitlab.com/gitlab-org/gitlab/-/issues/383997

The following epic is intented to capture all the work involved in managing the complete lifecycle of these integration branches and ensuring all their code makes it to master correctly and efficiently: https://gitlab.com/groups/gitlab-org/-/epics/10258

## Integration branches and associated MRs

- `gitlab` project (rails monolith):
  - Branch: https://gitlab.com/gitlab-org/gitlab/-/tree/remote_dev
  - MR: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/105783+
- `gitlab-agent` project (golang/protobuf portions of [GitLab Agent for Kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/)):
  - Branch: https://gitlab.com/gitlab-org/gitlab-development-kit/-/tree/remote_dev
  - MR: https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/805+
- `gitlab-development-kit` project:
  - Branch: https://gitlab.com/gitlab-org/gitlab-development-kit/-/tree/remote_dev
  - MR: https://gitlab.com/gitlab-org/gitlab-development-kit/-/merge_requests/2863+ 

## Process

## Process for new feature work on the integration branches

It's OK to not follow all of the code guideline standards. You should strive for writing at least happy-path tests and 100% test coverage (the `undercoverage` job will fail if you don't), and following all standards. 

It's fine to leave something difficult or time-consuming incomplete for a follow-up issue in order to get the basic functionality available in the integration branch, as long as it doesn't break any existing functionality or tests.

But, as we near the MVC, we should work on more polish, and getting everything into a high-quality state in preparation for merging the functionality to master in separate MRs against master. Ideally, we will have minimal changes needed in these MRs based on review feedback. More details on this in the section below.

1. Make the change:
    1. For larger/planned features, ensure there is an issue proritized in the current iteration , and create an associated MR in a 1-to-1 issue-to-MR relationship. This is per our [team's documented process](https://about.gitlab.com/handbook/engineering/development/dev/create/ide/#milestone-planning-and-remote-development-iteration-planning)
    1. For tiny fixes that you are _SURE_ won't break the build, you can commit directly to the `remote_dev` branch, but watch to make sure you didn't accidentally break the build.
1. If you created an Issue+MR, set the `target_branch` of the MR to `remote_dev`, and apply all the appropriate labels and add the issue to the appropriate epic. Especially important is the `Deliverable` label on the issue, so it will show up on our [Iteration Board](https://gitlab.com/groups/gitlab-org/-/boards/5283620?label_name[]=Category%3ARemote%20Development&label_name[]=Deliverable)
1. Depending on the scope of the feature, you can request a review for the MR, or you can self-merge it if the build passes anad you are confident of the changes. Ignore the "self-merge" warning, it should not be enabled for non-default-branch merges, and [there's an issue to fix this](https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/1302)).

### Important note on rebasing branches which are based on a regularly-rebased branch

**IMPORTANT: If you need to rebase your MR branch which is based off of any upstream branch which may have been rebased since your branch was created, you should use the `--onto` option for rebase. This applies to all MR branches created off with the `remote_dev` branch as the target**

This is because we will be when you rebase the upstream integration branch (e.g. `remote_dev`) against _ITS_ upstream branch (e.g. the default branch, `main` or `master`), all the SHAs will change on the upstream integration branch, and then `git` can't automatically figure out what the proper [merge-base](https://git-scm.com/docs/git-merge-base) is.

So, you have to tell it what the "previous" merge-base was. Here's how:

1. Ensure you have fetched the upstream integration `remote_dev` branch. Just check it out and do a `git pull`.
1. Change back to your MR branch, do a `git log`, and look for the _first_ commit that is _before_ all the commits on your branch. This is the "_previous_ `merge-base`". Copy the SHA for this commit.
  1. Alternately, just calculate the correct `head~n`/`head^` relative ref, and not bother with the log or copying a SHA. E.g., if you know you keep your branch always squashed to a single commit, you can just use `head~1` or `head^` as the previous merge-base.
1. Rebase your MR branch with the following command: `git rebase <previous merge-base SHA> --onto <upstream branch name>`, e.g. `git rebase f83e43e51f2361c926293801ae8c9b5853f641d3 --onto remote_dev`. This will ensure that _ONLY_ the commits on your branch are re-applied _RELATIVE_ to the upstream branch.

## Process for ensuring all integration branch work is eventually merged to master

(This section was based on the ideas in [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/404552))

Ideally, we want to clean up everything while it is still on the `remote_dev` branch, and have it in a completed and final state, and curate it (via interactive rebase) to single, clean commits which can be cherry-picked when we start the master MRs.

In other words: Keep the `remote_dev` branch curated out to a clean set of separate commits which represent the individual MRs that we will merge to master.

This will also ensure that we have all the dependencies clearly delineated before we start the work on the master MRs. When we split up the commits like this, the nature of `git` will force us to ensure that all MRs can be applied sequentially, and that there are no circular dependencies, because `git` is a [directed acyclic graph](https://en.wikipedia.org/wiki/Directed_acyclic_graph).

We _MAY_ be able to do some master MRs in parallel, such as Vue frontend work vs. core domain logic under `ee/lib/remote_development`, but we will need to be careful about ensuring there are no missing dependencies (e.g. that a given GraphQL type/mutation/schema is stable, merged, and no longer changing on the integration branch, _prior_ to starting a frontend MR which depends upon it).

### Master MR process summary

This process is a bit involved, and will require some git-fu, but our goal is to be very disciplined and methodical about the up-front planning about exactly what is going to go into each of the various master MRs, and ensure there are no unexpected delays or confusion/blockers due to circular dependencies/etc.

NOTE: `master` used to refer to default branch in the following instructions, even though it may be `main`

1. When we are ready to start making MRs for master, do an interactive rebase with `edit` to split up the `remote_dev` branch into curated commits corresponding to planned master MRs. We can refer to [this doc](https://docs.google.com/document/d/1TuB40gFxeJuN6UxCsxCKEgEOl6zC8KYhgU4hEd7IGqc/edit), or whatever sub-epics of [the MVC epic](https://gitlab.com/groups/gitlab-org/-/epics/10122) result from it, for inspiration on how to split these up. Although we may not be able to follow that strictly, as the logical dependency-order may span across areas of functionality.
1. Make a new master MR by cherry-picking a curated `remote_dev` commit to a new MR branch off of master, and start the MR review. Ensure it is added to https://gitlab.com/groups/gitlab-org/-/epics/10258
1. **Master MRs should _NOT_ be squashed!**. Any changes on master MRs which are needed due to review feedback will be kept in separate commits, so we can cherry-pick them back to the `remote_dev` branch (details below). Then, once the master MR is merged, all of the corresponding commits that were merged as part of the MR should auto-magically disappear the next time we rebase the remote_dev branch (because they are identical commits). You _can_ set the "squash on merge" checkbox on the MR (I don't _believe_ this will confuse git's ability to auto-magically detect the corresponding commits during the remote_dev branch rebase, but I could be wrong. If so, we can ensure we don't squash on merge to master so the commits/SHAs don't change).
    1. Alternately, you could wait until all the review feedback is complete, then squash it to a single commit (but NOT squash it into the original curated commit), then interactive-rebase that single commit back to the proper position in the `remote_dev` integration branch. This might be preferable, since it will result in fewer total commits on the integration branch and make it easier for us to stay below 20 (see below).
1. As we continue to make changes on the master MRs (based on review feedback), we will cherry-pick those changes back into the proper place for their corresponding commits on the `remote_dev` branch via interactive rebase. E.g., they will be cherry picked right after their corresponding commit.
   1. NOTE: Ideally we will not squash these commits down while. But, once you get more than 20 commits the build stops failing. So, we will see if we can stay under 20, and if we can't, we'll either decide to squash or live with the failing build. TODO: Can we disable this danger rule for our branch somehow?
1. As we continue to make changes on the `remote_dev` branch for new MVC features/functionality (via the "Process for new feature work on the integration branches" described in the section above), we can ensure those are also incorporated into individual curated commits, either squashed to an existing curated commit, or to a new one if it's a new cohesive piece of work that will have a dedicated master MR.
1. We should ultimately end up with nothing on the remote_dev branches at all. All code should be merged as part of MRs or deleted. Any TODO comments should have a corresponding issue opened, put the issue URL in the TODO (per our doc guidelines), and then merge the TODOs to master in an MR (or just delete them if we are OK with just the issue).

**_TODO: Come up with a standardized naming prefix for the various curated/picked commit messages, to make it easier to keep track of them._**