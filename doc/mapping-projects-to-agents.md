# Mapping projects to agents

[[_TOC_]]

## Problem

### User selecting project-agent pair

The user wants to create a workspace for their project. They need to select an agent to provision the workspaces. The problem is which agents we should offer them.

### Admin adding an agent to the GitLab instance/group/project

Where should an admin/maintainer configure the availability of an agent for creating new workspaces?

## Solution

This section describes how we can achieve the following setup where agents can support creating workspaces in multiple projects.

```mermaid
flowchart LR
    subgraph "possibly entirely different group"
    A[configuration project]
    end
    subgraph "gitlab-org group"
    D[Project with workspaces]
    E[Project with workspaces]
    end
    A -. configures .-> B[Agent]
    D -- creates WS --> B
    E -- creates WS -->  B
    B --> C[K8S cluster]
    subgraph "gitlab-org/sub group"
    F[Project with workspaces]
    end
    F-- creates WS -->  B
```

### Interim solution

This solution is only for the **internal/closed** beta. It makes it possible for us to release _something_ whilst we still implement the "Complete solution".

The interim solution is to use the existing GraphQL query. The query doesn't consider whether the Remote Development feature is on. So we'll have to _manually_ ensure that all available agents have the feature enabled (or the user gets an error when creating a workspace).

The query gives us two options:

- Get agents in a group - This query returns all agents defined under the group. With this option, we would have to create a new "remote development enabled" group, add a "remote development enabled" agent, and then all its projects would have access to the agent.
  - This option enables **creating many new projects without the need to change an agent**.
- Get agents in a project - This query returns all agents defined in this project. With this option, we would add a "remote development enabled" agent to each project that can create workspaces.
  - This option enables **adding remote development to existing projects (like `www-gitlab-com`) without the need to move them to a different group**.

| Get agents in a group                                                                                                                               | Get agents in a project                                                                                                                                             |
| --------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![guarantee-rd-enabled-for-agent](https://gitlab.com/gitlab-org/gitlab/uploads/78561407fae8f4c135e8eea936027bdb/guarantee-rd-enabled-for-agent.png) | ![rd-enabled-for-agent-in-single-project](https://gitlab.com/gitlab-org/gitlab/uploads/93d9014885a4e2abd8a9d595bd8810eb/rd-enabled-for-agent-in-single-project.png) |

I slightly prefer creating a separate group and if we want to work with a "immovable" project like `gitlab` or `www-gitlab-com` in the internal beta, we can fork them there.

There wouldn't be an administration screen showing group agents.

### Complete solution

The complete solution allows the administrator to specify which groups and projects can use the agent to create a new workspace (same scopes as CI/CD). This configuration will be in the `config.yml` file for each agent.

```yml
remote_development:
  dns_zone: workspaces.gitlab.io
  access:
    groups:
      - id: gitlab-org
    projects:
      - id: group/project-name
```

Only projects and groups from the agent configuration project's _root group_ can be configured. In other words, if the agent is configured in `g1/p1` project, the agent configuration can give access to `g1` group, `g1/g2/g3` group, but not to `gitlab-org`, because `gitlab-org` is not under the `g1` root group[^1].

[^1]: This behaviour is identical to the CI Access for agents. See the documentation [Authorize the agent to access your projects](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html#authorize-the-agent-to-access-your-projects) (_Authorized projects must have the same root group or user namespace as the agent's configuration project_)

Once this configuration is committed, Rails will link the agent and the group/project. When the user wants to create a workspace for a project, we'll find all agents that can be accessed by the project and all its parent groups.

Two conditions have to be met:

- User has developer access to the **project**
- The agent is configured for the given project or its parent groups

<details><summary>Detailed enumeration of permissions</summary>

The following scenarios illustrate how we would map user permissions with remote development access configuration:

```yml
# setup remote development for the agent
remote_development:
  dns_zone: workspaces.gitlab.io
  access:
    groups:
      - id: gitlab-org
      - id: group1/sub-group
    projects:
      - id: group2/project2
```

- **Scenario 1.1**
  - If the user tries to create a workspace
    - from any project under group `gitlab-org` or `group1/sub-group`
    - has at least developer access to the actual project(in which the workspace is being created)
  - then
    - the agent will be visible in the list of agents dropdown
    - user would be able to create the workspace
- **Scenario 1.2**
  - if the user tries to create a workspace
    - from any project under group `gitlab-org` or `group1/sub-group`
    - **DOES NOT** have at least developer access to the actual project(in which workspace is being created)
  - then
    - user will not be allowed to create any workspace for this project
- **Scenario 2.1**
  - if the user tries to create a workspace
    - from project `group2/project2`
    - has developer access to the actual project(in which workspace is being created)
  - then
    - the agent will be visible in the list of agents dropdown
    - user would be able to create the workspace
- **Scenario 2.2**
  - if the user tries to create a workspace
    - from project `group2/project2`
    - **DOES NOT** have developer access to the actual project(in which workspace is being created)
  - then
    - user will not be allowed to create any workspace for this project
- **Scenario 3.1**
  - if the user tries to create a workspace
    - from project `yet-another-root-group-namespace/project2`
    - has developer access to the actual project(in which workspace is being created)
  - then
    - the agent will NOT BE visible in the list of agents dropdown
- **Scenario 3.2**
  - if the user tries to create a workspace
    - from project `yet-another-root-group-namespace/project2`
    - **DOES NOT** have developer access to the actual project(in which workspace is being created)
  - then
    - user will not be allowed to create any workspace for this project

</details>

Users do not need developer access to the group to utilize the group agent. It is sufficient they have developer access to the project. The GitLab CI/CD runner permissions work the same way.

We'll have to implement UI screens showing the available agents for group/project - similar to how we currently do it with CI/CD configuration.

#### Lovable features

- Adding **instance** agents (in CI/CD it's called _shared runners_). We can add them later when we consider adding Remote Development clusters to SaaS.

- Disabling instance/group agents for a project
  - This would be useful if a project needs a specific cluster for its workspaces
  - This is how CI/CD runners work

## Technical details

### Current way Agents are modelled in Rails

Currently, the agent is set up in a configuration project. This project contains a configuration file for an agent. This configuration connects the project to the kubernetes cluster. The configuration can specify that other projects have access to the agent:

- [`user_access`](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/kubernetes_user_access.md) - allows users with developer access to projects/groups to interact directly with a cluster
- [`ci_access`](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/kubernetes_ci_access.md) - allows CI jobs from defined projects/groups to access the cluster (these projects/groups don't have to be in the same group as the configuration project) - [user docs](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html#authorize-the-agent)

```yaml
ci_access:
  projects:
    - id: path/to/project
  groups:
    - id: path/to/group/subgroup
```

```mermaid
flowchart LR
    A[Configuration project] -- has --> B[Agent]
    B --> C[Kubernetes cluster]
    D[Project with CI jobs accessing cluster] --> F[ProjectAuthorization]
    F --> B
    E[Group with CI jobs accessing cluster] --> G[GroupAuthorization]
    G --> B
```

### The existing GraphQL query to get agents

#### Query behaviour

- The user has to be a `Developer` member of a group or a project.
- The endpoint returns only agents that are configured in a configuration project.
- The configuration project has to be in the group or its subgroups.
- We can't filter agents that have RemoteDevelopment feature enabled.
- If we want to list agents for a project (and not a group) we would only see agents that are _configured in the project_.
- **no screen shows the overview of "group agents"** (unlike CI/CD runners `http://gdk.test:3000/groups/gitlab-org/-/runners`)

#### Query details

There is a GraphQL query we use to get all agents in a group:

```graphql
query getAgents {
  group(fullPath:"gitlab-org"){
    clusterAgents{
      nodes {
        name
      }
    }
  }
}
```

The model method used is `ee/app/models/ee/group.rb`.`cluster_agents`, which returns agents in all configuration projects in this group and its subgroups.

This query was introduced for security dashboards([Add clusterAgents field to Project/Group/Security Dashboard GraphQL API](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/89357)) and doesn't support filtering on "enabled features" on the agent. Otherwise, it works quite well:

This query returns agents only if you have **Developer access to the group you are querying**. (e.g. in a `group1/subgroup2` hierarchy, members of `subgroup2` (but not `group1`) can only query agents set up in `subgroup2`, members of `group1` can see agents from `subgroup2` and any other agents in the group hierarchy)

The visibility of the configuration project doesn't play a role (It can be public or private. Unless you are a group member with a developer role, you won't see the agents.). That means people creating workspaces _in our spike_ can also see agent configuration (because the configuration project inherits members from the group).

### Implementation details for the Complete solution

The `ci_access` feature uses two models aka "lookup tables" [`Clusters::Agents::GroupAuthorization` and `Clusters::Agents::ProjectAuthorization`](https://gitlab.com/gitlab-org/gitlab/blob/521da864424cbc9ac3e930379d8d3acd29dc0125/app/models/clusters/agent.rb#L15-19). These are created based on the `ci_access` configuration in the agent's `config.yml` file. We will have to **implement a new association (e.g. `RemoteDevelopmentGroupAuthorization`, `RemoteDevelopmentProjectAuthorization`)**. We also considered reusing the existing `GroupAuthorization`, And renaming the `GroupAuthorization` to `CiGroupAuthorization`. The detailed reasoning is in [this discussion thread](https://gitlab.com/gitlab-org/remote-development/gitlab-remote-development-docs/-/merge_requests/19#note_1325610490).

We extract this `ci_access` configuration in rails in [`app/services/clusters/agents/refresh_authorization_service.rb`](https://gitlab.com/gitlab-org/gitlab/blob/521da864424cbc9ac3e930379d8d3acd29dc0125/app/services/clusters/agents/refresh_authorization_service.rb#L67-77)

#### Tricky parts of the implementation

There are now multiple ways of getting agents for a group:

- GraphQL query which gets all agents from all configuration projects in the group or its subgroups (used for security dashboard)
- The `GroupAuthorization` relation used for `ci_access` (used for CI Jobs accessing clusters)

None of these is namespaced with the feature (ci/security), and we might create a third type of association. It would be nice to rename `GroupAuthorization` to `CiGroupAuthorization`, but it [might be too much effort to rename the table](https://gitlab.com/gitlab-org/remote-development/gitlab-remote-development-docs/-/merge_requests/19#note_1330594751).

## CI Runner business rules (how could we inspire our business rules)

A runner can be created in three _scopes_:

- [Shared runners](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#shared-runners) are for use by all projects
  - These are configured per instance
  - Work is assigned by the "fair usage" algorithm (the project with the least jobs already running will get its jobs scheduled)
- [Group runners](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#group-runners) are for all projects and subgroups in a group
  - All projects in the group have access to the group runners
  - Group runners use FIFO queue for processing jobs
  - DONE do we inherit runners from all parent groups?
  - groups inherit runners from parent groups
  - group runners are available to all projects within the group, but projects can decide to disable the group runners and use only project runners
- [Project runners](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#project-runners) are for individual projects

Settings screens:

| instance                                                                                                                                                | group                                                                                                                                                   | project                                                                                                                                                 |
| ------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![Screenshot_2023-03-17_at_8.08.16](https://gitlab.com/gitlab-org/gitlab/uploads/04ffa4e63eb31bcb8419527a57333e79/Screenshot_2023-03-17_at_8.08.16.png) | ![Screenshot_2023-03-17_at_8.08.58](https://gitlab.com/gitlab-org/gitlab/uploads/719bdec035bf782ff7d628b5d3398142/Screenshot_2023-03-17_at_8.08.58.png) | ![Screenshot_2023-03-17_at_8.10.16](https://gitlab.com/gitlab-org/gitlab/uploads/78896ca369bb13d6c972a015e53993b8/Screenshot_2023-03-17_at_8.10.16.png) |

Jobs are matched with runners based on tags. You [add tags to a runner when registering it](https://docs.gitlab.com/ee/api/runners.html#register-a-new-runner), tags can't be changed after runner registration.

The permissions for interacting with CI/CD are described in [GitLab CI/CD Permissions](https://docs.gitlab.com/ee/user/permissions.html#gitlab-cicd-permissions). The two permissions that are important for us are:

- Run CI/CD pipeline - Developer, Maintainer, and Owner
- Manage CI/CD settings - Maintainer and Owner
