# GitLab Remote Development User Stories

The [personas in action](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#user-personas)
for Remote Development at GitLab are:

- [Sasha, the Software Developer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer).
- [Allison, the Application Operator](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#allison-application-ops).
- [Delaney (Development Team Lead)](https://about.gitlab.com/handbook/product/personas/#delaney-development-team-lead).

## Software Developer user stories

- As a Software Developer, I want to be able to quickly set up and configure a development environment in a GitLab workspace, so that I can start coding right away without having to spend time configuring my own environment.
- As a Software Developer, I want to be able to collaborate with other developers in GitLab Workspaces, so that we can work together efficiently and effectively on projects.
- As a Software Developer, I want to be able to access all of the tools and resources I need to work on my code, such as debugging tools and language server features, directly within GitLab Workspaces, so that I can work more efficiently and effectively.
- As a Software Developer, I want to be able to use GitLab Workspaces to test and debug my code on different CPU architectures, so that I can ensure compatibility and fix any issues that arise.
- As a Software Developer, I want to be able to use GitLab Workspaces to easily switch between different branches and versions of my code, so that I can easily test and debug different versions of my project.
- As a Software Developer, I want to be able to use GitLab Workspaces to easily collaborate with other members of my team, such as other developers, project managers, and QA testers, so that we can work together efficiently and effectively to deliver high-quality software.
- As a Software Developer, I want to be able to easily access the latest version of the codebase in GitLab Workspaces, so that I can ensure that my team is working on the most up-to-date version of the project.

## Application Operator user stories

- As an Application Operator, I want to be able to quickly set up and manage development environments in GitLab Workspaces, so that developers can start coding right away for projects which have an workspace environment configuration.
- As an Application Operator, I want to be able to replicate production issues in GitLab Workspaces, so that I can test and debug code in a controlled environment before deploying it to productio
- As an Application Operator, I want to be able to use GitLab Workspaces to easily test code changes in different environments, such as staging or production, so that I can ensure compatibility and fix any issues that arise.
- As an Application Operator, I want to be able to use GitLab Workspaces to easily switch between different branches and versions of code, so that I can easily test and debug different versions of the application.
- As an Application Operator, I want to be able to use GitLab Workspaces to automatically run tests and perform other checks on code before it is deployed, so that I can ensure that the code is stable and reliable.
- As an Application Operator, I want to be able to use GitLab Workspaces to easily collaborate with other members of my team, such as developers and other SREs, so that we can work together efficiently and effectively to ensure that our code is stable and reliable.
- As an Application Operator, I want idle workspaces to automatically terminate after x amount of time to save compute resoruces.

## Development Team Lead user stories

- As a Development Team Lead, I want to be able to easily set up and manage development environments for my team members in GitLab Workspaces, so that they can start coding right away without having to spend time configuring their own environment.
- As a Development Team Lead, I want to be able to provide feedback and guidance to my team members in real-time within GitLab Workspaces, so that we can work together efficiently and effectively on projects.
- As a Development Team Lead, I want to be able to easily share and access different parts of the codebase with other team members in GitLab Workspaces, so that we can work on different aspects of the project simultaneously.
- As a Development Team Lead, I want to be able to easily switch between different branches and versions of code in GitLab Workspaces, so that I can review and approve changes made by my team members.
- As a Development Team Lead, I want to be able to easily collaborate with other members of my team, such as developers and other team leads in GitLab Workspaces, so that we can work together efficiently and effectively to ensure that our code is stable and reliable.

