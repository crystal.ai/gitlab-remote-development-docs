# Workspace Updates

[[_TOC_]]

This document aims to show how different events change the database records and how the communication between `rails` and `agentk` reflects these events.

There are two kinds of workspace updates - `full` and `partial`. See the [Types of messages](https://gitlab.com/gitlab-org/remote-development/gitlab-remote-development-docs/-/blob/main/doc/architecture.md#types-of-messages) documentation for more information.

## Modelling of different scenarios that can occur for Partial Sync

- `request` - The event that occurs. `CURRENT_DB_STATE` describes the state of the DB before any event occurs.
- `include config_to_apply in workspace_rails_info response?`
  - For events - `CURRENT_DB_STATE` and `USER_ACTION` is this not applicable. This is signified by a `-`.
  - For events - `AGENT_ACTION` - Based on the state of the DB before the DB is updated by the information received from agentk about the workspace, should Rails send the `config_to_apply` information to Agentk based on the condition `desired_state_updated_at >= responded_to_agent_at`?
- `include deployment_resource_version in workspace_rails_info response?` - Should Rails send the `deployment_resource_version` information to Agentk?
  - We've chosen to not show this field in the table below for the sake of brevity as it will always evaluate to true in teh below scenarios(except for the [one scenario](#no-update-for-workspace-from-agentk-or-from-user) where it is called out)
  - Rules of evaluation
    - If we received information about the workspace from agentk, then Yes
    - If we are sending config to apply for the workspace to agentk, then Yes
    - Else, No
- `desired_state_updated_at`, `responded_to_agent_at`, `desired_state`, `actual_state` - The state of the DB after the event occurs
- `responded_to_agent_at` is always updated to the current timestamp when we respond to agent with information about the workspace. The information can include `config_to_apply` or `deployment_resource_version` or both

NOTE:
- We have an [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/406710) open to not set the `actual_state` of the workspace to `Creating` when a new workspace is requested. Thus, all references to `Creating` below are followed by an asterisk `*`.
- We have an [issue] open to discuss removal of `Creating` actual_state.

#### desired: Running / actual: Creating -> desired: Running / actual: Running

* New Workspace is requested by the user which results in a Running actual state

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Empty DB | - |  |  |  |  |
| USER_ACTION - User has requested a new workspace | - | Running | Creating | 05:00 |  |
| AGENT_ACTION - Agentk reports no info | Y | Running | Creating | 05:00 | 05:01 |
| AGENT_ACTION - Agentk reports it as Starting | N | Running | Starting | 05:00 | 05:02 |
| AGENT_ACTION - Agentk reports it as Running | N | Running | Running | 05:00 | 05:03 |

#### desired: Running / actual: Creating -> desired: Running / actual: Failed

* New Workspace is requested by the user which results in a Failed actual state. e.g. the container is crashing.

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Empty DB | - |  |  |  |  |
| USER_ACTION - User has requested a new workspace | - | Running | Creating | 05:00 |  |
| AGENT_ACTION - Agentk reports no info | Y | Running | Creating | 05:00 | 05:01 |
| AGENT_ACTION - Agentk reports it as Starting | N | Running | Starting | 05:00 | 05:02 |
| AGENT_ACTION - Agentk reports it as Failing | N | Running | Failed | 05:00 | 05:03 |

#### desired: Running / actual: Creating -> desired: Running / actual: Error

* New workspace is requested by the user which results in a Error actual state. e.g. failed to apply kubernetes resources.

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Empty DB | - |  |  |  |  |
| USER_ACTION - User has requested a new workspace | - | Running | Creating | 05:00 |  |
| AGENT_ACTION - Agentk reports no info | Y | Running | Creating | 05:00 | 05:01 |
| AGENT_ACTION - Agentk reports it as Error | N | Running | Error | 05:00 | 05:02 |

---

#### desired: Running / actual: Running -> desired: Stopped / actual: Stopped

* Running workspace is stopped by the user which results in a Stopped actual state.

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Running state | - | Running | Running | 05:00 | 05:01 |
| USER_ACTION - User stops the workspace | - | Stopped | Running | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Stopped | Running | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Stopping | N | Stopped | Stopping | 05:02 | 05:04 |
| AGENT_ACTION - Agentk reports it as Stopped | N | Stopped | Stopped | 05:02 | 05:05 |

#### desired: Running / actual: Running -> desired: Stopped / actual: Failed

* Running workspace is stopped by the user which results in a Failed actual state. e.g. could not unmount volume and stop the workspace

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Running state | - | Running | Running | 05:00 | 05:01 |
| USER_ACTION - User stops the workspace | - | Stopped | Running | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Stopped | Running | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Stopping | N | Stopped | Stopping | 05:02 | 05:04 |
| AGENT_ACTION - Agentk reports it as Failed | N | Stopped | Failed | 05:02 | 05:05 |

#### desired: Running / actual: Running -> desired: Stopped / actual: Error

* Running workspace is stopped by the user which results in a Error actual state. e.g. failed to apply kubernetes resources

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Running state | - | Running | Running | 05:00 | 05:01 |
| USER_ACTION - User stops the workspace | - | Stopped | Running | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Stopped | Running | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Error | N | Stopped | Error | 05:02 | 05:04 |

---

#### desired: Running / actual: Running -> desired: Terminated / actual: Terminated

* Running workspace is terminated by the user which results in a Terminated actual state.

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Running state | - | Running | Running | 05:00 | 05:01 |
| USER_ACTION - User terminates the workspace | - | Terminated | Running | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Terminated | Running | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Terminated | N | Terminated | Terminated | 05:02 | 05:04 |

#### desired: Running / actual: Running -> desired: Terminated / actual: Failed

* Running workspace is terminated by the user which results in a Failed actual state. e.g. could not unmount volume and terminate the workspace

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Running state | - | Running | Running | 05:00 | 05:01 |
| USER_ACTION - User terminates the workspace | - | Terminated | Running | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Terminated | Running | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Failed | N | Terminated | Failed | 05:02 | 05:04 |

#### desired: Running / actual: Running -> desired: Terminated / actual: Error

* Running workspace is terminated by the user which results in a Terminated actual state. e.g. failed to apply kubernetes resources

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Running state | - | Running | Running | 05:00 | 05:01 |
| USER_ACTION - User terminates the workspace | - | Terminated | Running | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Terminated | Running | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Error | N | Terminated | Error | 05:02 | 05:04 |

---

#### desired: Stopped / actual: Stopped -> desired: Running / actual: Running

* Stopped workspace is started by the user which results in a Running actual state.

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Stopped state | - | Stopped | Stopped | 05:00 | 05:01 |
| USER_ACTION - User starts the workspace | - | Running | Stopped | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Running | Stopped | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Starting | N | Running | Starting | 05:02 | 05:04 |
| AGENT_ACTION - Agentk reports it as Running | N | Running | Running | 05:02 | 05:05 |

#### desired: Stopped / actual: Stopped -> desired: Running / actual: Failed

* Stopped workspace is started by the user which results in a Failed actual state. e.g. container is crashing.

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Stopped state | - | Stopped | Stopped | 05:00 | 05:01 |
| USER_ACTION - User starts the workspace | - | Running | Stopped | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Running | Stopped | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Starting | N | Running | Starting | 05:02 | 05:04 |
| AGENT_ACTION - Agentk reports it as Failed | N | Running | Failed | 05:02 | 05:05 |

#### desired: Stopped / actual: Stopped -> desired: Running / actual: Error

* Stopped workspace is started by the user which results in a Error actual state. e.g. failed to apply kubernetes resources.

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Stopped state | - | Stopped | Stopped | 05:00 | 05:01 |
| USER_ACTION - User starts the workspace | - | Running | Stopped | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Running | Stopped | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Error | N | Running | Error | 05:02 | 05:04 |

---

#### desired: Stopped / actual: Stopped -> desired: Terminated / actual: Terminated

* Stopped workspace is terminated by the user which results in a Terminated actual state.

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Stopped state | - | Stopped | Stopped | 05:00 | 05:01 |
| USER_ACTION - User terminates the workspace | - | Terminated | Stopped | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Terminated | Stopped | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Terminated | N | Terminated | Terminated | 05:02 | 05:04 |

#### desired: Stopped / actual: Stopped -> desired: Terminated / actual: Failed

* Stopped workspace is terminated by the user which results in a Failed actual state. e.g. could not unmount volume and terminate the workspace

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Stopped state | - | Stopped | Stopped | 05:00 | 05:01 |
| USER_ACTION - User terminates the workspace | - | Terminated | Stopped | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Terminated | Stopped | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Failed | N | Terminated | Failed | 05:02 | 05:04 |

#### desired: Stopped / actual: Stopped -> desired: Terminated / actual: Error

* Stopped workspace is terminated by the user which results in a Error actual state. e.g. failed to apply kubernetes resources

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Stopped state | - | Stopped | Stopped | 05:00 | 05:01 |
| USER_ACTION - User terminates the workspace | - | Terminated | Stopped | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Terminated | Stopped | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Error | N | Terminated | Error | 05:02 | 05:04 |

---

#### desired: Running / actual: Failed -> desired: Running / actual: Running

* Failed workspace becomes ready which results in a Running actual state. e.g. container is no longer crashing

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Failed state | - | Running | Failed | 05:00 | 05:01 |
| AGENT_ACTION - Agentk reports it as Starting | N | Running | Starting | 05:00 | 05:02 |
| AGENT_ACTION - Agentk reports it as Running | N | Running | Running | 05:00 | 05:03 |

---

#### desired: Running / actual: Failed -> desired: Stopped / actual: Stopped

* Failed workspace is stopped by the user which results in a Stopped actual state.

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Failed state | - | Running | Failed | 05:00 | 05:01 |
| USER_ACTION - User stops the workspace | - | Stopped | Failed | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Stopped | Failed | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Stopping | N | Stopped | Stopping | 05:02 | 05:04 |
| AGENT_ACTION - Agentk reports it as Stopped | N | Stopped | Stopped | 05:02 | 05:05 |

#### desired: Running / actual: Failed -> desired: Stopped / actual: Failed

* Failed workspace is stopped by the user which results in a Failed actual state. e.g. could not unmount the volume and stop the workspace

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Failed state | - | Running | Failed | 05:00 | 05:01 |
| USER_ACTION - User stops the workspace | - | Stopped | Failed | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Stopped | Failed | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Failed | N | Stopped | Failed | 05:02 | 05:04 |

#### desired: Running / actual: Failed -> desired: Stopped / actual: Error

* Failed workspace is stopped by the user which results in a Failed actual state. e.g. failed to apply kubernetes resources

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Failed state | - | Running | Failed | 05:00 | 05:01 |
| USER_ACTION - User stops the workspace | - | Stopped | Failed | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Stopped | Failed | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Error | N | Stopped | Error | 05:02 | 05:04 |

---

#### desired: Running / actual: Failed -> desired: Terminated / actual: Terminated

* Failed workspace is terminated by the user which results in a Terminated actual state.

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Failed state | - | Running | Failed | 05:00 | 05:01 |
| USER_ACTION - User terminates the workspace | - | Terminated | Failed | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Terminated | Failed | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Terminated | N | Terminated | Terminated | 05:02 | 05:04 |

#### desired: Running / actual: Failed -> desired: Terminated / actual: Failed

* Failed workspace is terminated by the user which results in a Failed actual state. e.g. could not unmount volume and terminate the workspace.

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Failed state | - | Running | Failed | 05:00 | 05:01 |
| USER_ACTION - User terminates the workspace | - | Terminated | Failed | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Terminated | Failed | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Failed | N | Terminated | Failed | 05:02 | 05:04 |

#### desired: Running / actual: Failed -> desired: Terminated / actual: Error

* Failed workspace is terminated by the user which results in a Error actual state. e.g. failed to apply kubernetes resources

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Failed state | - | Running | Failed | 05:00 | 05:01 |
| USER_ACTION - User terminates the workspace | - | Terminated | Failed | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Terminated | Failed | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Error | N | Terminated | Error | 05:02 | 05:04 |

---

#### desired: Running / actual: Error -> desired: Stopped / actual: Error

* Error workspace is stopped by the user which results in a Error actual state. e.g. failed to apply kubernetes resources
* Not sure if this transition should be allowed.

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in error state | - | Running | Error | 05:00 | 05:01 |
| USER_ACTION - User stops the workspace | - | Stopped | Error | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Stopped | Error | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as error | N | Stopped | Error | 05:03 | 05:04 |

#### desired: Running / actual: Error -> desired: Terminated / actual: Error

* Error workspace is terminated by the user which results in a Error actual state. e.g. failed to apply kubernetes resources
* Not sure if this transition should be allowed. Need to think if we are leaving some uncleaned state if we don't allow this. What is the workspace was never created in the first place? What is the workspace was successfully created but entered the Error state while it was being stopped?

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in error state | - | Running | Error | 05:00 | 05:01 |
| USER_ACTION - User terminates the workspace | - | Terminated | Error | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Terminated | Error | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as error | N | Terminated | Error | 05:03 | 05:04 |

---

### Other scenarios

#### Agent reports update for a workspace and user has also updated desired state of the workspace

- This is to showcase a scenario where the agentk is reporting on a workspace and the user has also performed some action on the workspace.
- This was to highlight that this logic works even in cases like these where there is new information on both sides(agent and rails).
- Details of the scenario -
  - Workspace was earlier running
  - Workspace has now started crashing(i.e. Failed state)
  - User stops the workspace because they no longer need it (without being aware that the workspace has started crashing).

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Running state | - | Running | Running | 05:00 | 05:01 |
| USER_ACTION - User stops the workspace | - | Stopped | Running | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports it as Failed | Y | Stopped | Failed | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Stopping | N | Stopped | Stopping | 05:02 | 05:04 |
| AGENT_ACTION - Agentk reports it as Stopped | N | Stopped | Stopped | 05:02 | 05:05 |

#### Restarting a workspace

* Running workspace is restarted by the user which results in a Running actual state.

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Running state | - | Running | Running | 05:00 | 05:01 |
| User restarts the workspace | - | Restarting | Running | 05:02 | 05:01 |
| AGENT_ACTION - Agentk reports no info | Y | Restarting | Running | 05:02 | 05:03 |
| AGENT_ACTION - Agentk reports it as Stopping | N | Restarting | Stopping | 05:02 | 05:04 |
| AGENT_ACTION - Agentk reports it as Stopped,<br/>desired_state automatically changed to Running | Y | Running | Stopped | 05:05 | 05:05 |
| AGENT_ACTION - Agentk reports it as Starting | N | Running | Starting | 05:05 | 05:07 |
| AGENT_ACTION - Agentk reports it as Running | N | Running | Running | 05:05 | 05:08 |

#### No update for workspace from agentk or from user

- Since rails will not be sending information about this workspace in the response to agentk
  - `include deployment_resource_version in workspace_rails_info response?` is set to `N`
  - `responded_to_agent_at` is not updated
  - `include deployment_resource_version in workspace_rails_info response?` is set to `N`

| request | include config_to_apply in workspace_rails_info response? | desired_state | actual_state | desired_state_updated_at | responded_to_agent_at |
|:---:|:---:|:---:|:---:|:---:|:---:|
| CURRENT_DB_STATE - Workspace is in Running state | - | Running | Running | 05:00 | 05:01 |
| AGENT_ACTION - Agentk reports no info | N | Running | Running | 05:00 | 05:01 |

---

We've not modelled for `Unknown` actual state yet because we do not know when that would occur and what would happen. `Unkwon` is a fail safe state which should never occur ideally.
