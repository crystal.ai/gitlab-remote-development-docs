# Workspace DB design

[[_TOC_]]

A `Workspace` is a container/VM-based developer machine providing all the tools and dependencies needed to code, build, test, run, and debug applications.

This document explains how we model Workspace in Database and Why. This document is **not** an explanation of user requirements or UX of the Remote Development feature.

## Workspace database associations

In Rails DB, we associate each `Workspace` with the following entities. All associations are mandatory.

- `Group`
  - we do this for billing/accounting/management/etc purposes to a single `Group`,
  - `Group` is a `Namespace` (a `namespace` is the overall hierarchy of organization going forward, and a `group` is a type of a `namespace` and is a record in the `namespaces` table)
- `User`
  - `Workspace` belongs to a single user for now. We can allow a way for them to be shared or associated with multiple users if that makes sense in the future.
- `ClusterAgent`
  - `ClusterAgent` defines where the `Workspace` deploys and runs.
  - The relationship is _currently_ (in the spike) defined by a dropdown list of `ClusterAgents` to choose from when creating a `Workspace`. The agents in the dropdown are determined by looking for all `ClusterAgents` in the hierarchy of the `Group` associated with the `Workspace` (i.e. any sub-group or sub-project of the `Workspace’s` associated `group`.)
  - [Solve which agents we should offer to the user when creating Workspace](https://gitlab.com/gitlab-org/gitlab/-/issues/395115) issue will define what agents should be available for a workspace.
- `Project` and `Devfile SHA`
  - You need these two attributes to create a `Workspace` for a `Project` which contains a `Devfile`. Keeping track of which `Project` is associated with the `Workspace` will help certain UX elements, like showing which `Devfile` commit corresponds to the current version of `Workspace`.

## Associating Workspace with a Devfile in a project

As explained in the "Workspace database association" section, Workspace will be linked to a `Devfile` in a `Project`. By "Devfile in a Project" we mean a text file in a GitLab project. This text file contains the Devfile workspace specification. This link will be in the form of metadata attached to a `Workspace version`.

This link will help support any UX scenarios where the user wants to create a `Workspace` for a `Project` with a `Devfile` present (analogous to CI Pipeline being triggered for `Project` with `.gitlab-ci.yml` file).

There are mechanisms (Rails background jobs with [`ApplicationWorker` classes using `Sidekiq`](https://docs.gitlab.com/ee/development/sidekiq/)) in GitLab that allow us to react to Git push events to the Devfile. We could use this mechanism to notify users that their Workspace uses outdated Devfile.

The association of Workspace with a project is **mandatory**. We can make it optional in the future based on the result of [Consider scenarios where project-less Workspace might be beneficial](https://gitlab.com/gitlab-org/gitlab/-/issues/394858)

## Choosing to store the Workspace definition as a plain text field instead of a reference to a committed file

We choose to store the workspace definition as a plain text field. This is done to support _possible future scenarios_ where users created `Workspace`s that are not linked to a Devfile committed to a repository.

### Are workspaces going to have a single owner, or can we share workspaces with others?

Currently, `Workspace` has a single `User`, and we assume that will remain the case unless we have a product decision to change that. We don't expect adding multiple users to be difficult _from the database design perspective_.
