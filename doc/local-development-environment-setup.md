# Local Development Environment Setup

[[_TOC_]]

## Configure Kubernetes locally

- Install [Rancher.Desktop-1.8.1.aarch64.dmg](https://github.com/rancher-sandbox/rancher-desktop/releases/tag/v1.8.1)
  - Rancher 1.8.1 has some [installation regressions which can be mitigated](https://github.com/rancher-sandbox/rancher-desktop/issues/1815#issuecomment-1068522900) by running the following commands and restarting Rancher Desktop
    ```sh
    sudo mkdir -m 775 /private/var/run/rancher-desktop-lima
    sudo mkdir -p /private/etc/sudoers.d/
    sudo touch /private/etc/sudoers.d/zzzzz-rancher-desktop-lima
    ```
- Create a Kubernetes cluster `v1.26.3` with `contrainerd` container engine and uncheck the default selected `Enable Traefik` option

### Configure a load balancer

Install ingress-nginx:

```shell
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install \
  ingress-nginx ingress-nginx/ingress-nginx \
  --namespace ingress-nginx \
  --create-namespace \
  --version 4.3.0
```

Not used in the setup but is useful to know:

```shell
export LOAD_BALANCER_EXTERNAL_IP=$(
  kubectl -n ingress-nginx \
    get svc ingress-nginx-controller \
    --output jsonpath='{.status.loadBalancer.ingress[0].ip}'
)
```

## Configure GDK to use GitLab Agent for Kubernetes (GA4K)

1. [Install GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/tree/main#supported-methods).
1. [Set up an EE license](https://about.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee-developer-licenses).
1. Follow [doc/howto/local_network.md](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/local_network.md#local-network-binding) to set up your GDK to run on `172.16.123.1` IP address.
1. Follow [doc/howto/nginx.md](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/nginx.md) to install and configure NGINX for GDK.
1. Follow [doc/howto/kubernetes_agent.md](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/kubernetes_agent.md) to enable ga4k in your GDK.
1. [Register an agent in your GDK](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#register-the-agent-with-gitlab). Use any project. Registering an agent generates an **agent token**. You need this token to start an agent. Add the following agent configuration
    ```yaml
    remote_development:
      enabled: true
      dns_zone: workspaces.localdev.me
      # below configuration is optional
      # they override the default values
      # full_sync_interval: 30s
      # partial_sync_interval: 3s
      
      # for debugging locally to increase log verbosity
      # observability:
      #   logging:
      #     level: debug
      #     grpc_level: warn
    ```

## Configure GitLab Workspaces Proxy

1. Setup [GitLab Workspaces Proxy](https://gitlab.com/gitlab-org/remote-development/gitlab-workspaces-proxy#installation-instructions) to handle discovery, authentication and authorization of the workspaces running in the kubernetes cluster.

## Run GA4K locally (both kas and agentk)

`kas` is a server component that runs next to the GitLab, `agentk` is a client component that runs inside k8s cluster. For more info see the [architecture docs](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/architecture.md).

Follow the [Running kas and agentk locally](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/developing.md#running-kas-and-agentk-locally) section of agent development docs. Use your agent token instead of `<TOKEN>`.

## Testing

### Run E2E spec locally

- There is an end to end test that verifies the creation of a new running workspace
- The test works by running UI actions on a running installation of a test gitlab instance(using GDK), KAS and agentk
- The test **will not setup/teardown any of these components as a part of its execution**
- At present, the test is tagged with a `quarantine` label so it **will NOT run as a part of CI** due to complexities involved in spinning up KAS and agentk in the CI environment. As such it **must be run manually** for the time being

**Steps to run the test**
1. Ensure that the test gitlab instance is up and running with default KAS / agentk stopped
2. Ensure KAS with remote dev code is up and running
3. The e2e test, by default, assumes the existence of an agent with name `test-agent` under the group `gitlab-org` (available by default in the gdk gitlab instance)

   a. If you want to work with the defaults, create an agent with the name `test-agent` within a project in `gitlab-org` group. `gitlab-shell` project within the `gitlab-org` group is a possible candidate for where this agent can be created and this has been verified to work

   b. Alternatively if you wish to use a custom group/agent, it is possible to override the group and agent name used with the environment variables `AGENTK_GROUP` and `AGENTK_NAME` respectively
4. Change the current working directory to `{gdk directory}/gitlab`
5. Run the test with the following:

   `scripts/remote_development/run-e2e-spec.sh`

   OR if you wish to override the defaults

   `AGENTK_GROUP=some-org GITLAB_PASSWORD=example scripts/remote_development/run-e2e-spec.sh`

   The complete list of environment variables can be found in `scripts/remote_development/run-e2e-spec.sh`
