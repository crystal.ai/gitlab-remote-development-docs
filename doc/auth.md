# Workspace Authentication and Authorization

We need to only allow certain users to access workspaces. For MVC this is just going to be the creator of the workspace or the owner. DWO and Devfile Library out of the box do not provide the ability to authenticate or authorize a user, and therefore we need to build a proxy that will enable this critical functionality.

The proxy will proxy all HTTP and WebSocket calls to the correct workspace. It will perform the following tasks:

1. **Authentication**: It will use the [OAuth2 flow](https://docs.gitlab.com/ee/api/oauth2.html) with GitLab to authenticate the user. GitLab will act as the identity provider. If the customer uses a third party SSO service to log into GitLab, the flow would automatically delegate authentication to that provider. One of the complexities with authentication is the fact that each workspace is served on its own domain, and therefore we can't set the redirect URI on the GitLab app to a specific workspace. We need to set a state in the OAuth2 flow to redirect to the correct workspace.
2. **Authorization**: The proxy will make a call to a GitLab GraphQL endpoint with the user's credentials obtained in the authentication phase. The endpoint will validate if the user has access to the workspace and accordingly return either a 404 or a 200.
3. **Session Management**: We want the proxy to be stateless and be deployed without additional third party software such as a cache or database, and therefore we are using a signed JWT to manage sessions. The JWT is signed using a key provided to the proxy.

For the proxy there are two deployment mechanisms which have been described below. The preference of the team for the initial release we will be to use the Central Proxy design.

## Option 1: Central Proxy

In this design a single proxy will be created that will auto discover targets based on labels. The central proxy will watch the Kubernetes API for the creation / updation / deletion of service resources. When an service object is created, the central proxy will automatically configure itself to use corresponding service as an upstream.

Network access to the central proxy can be provided by using an ingress or by pointing a load balancer to the proxy. [Network policies](https://kubernetes.io/docs/concepts/services-networking/network-policies/) can be used to ensure that only the central proxy can reach the workspace ports. These network policies will have to be configured by the customer.

The central proxy will require a Kubernetes service account and a role that allows it to watch, list and get service resources.

```mermaid
flowchart TB
    Developer --> Ingress
    subgraph WorkspaceCluster[Workspace Cluster]
    Ingress --> AuthProxy[Auth Proxy]
    AuthProxy --Proxy--> Workspace1[Workspace 1]
    AuthProxy --Proxy--> Workspace2[Workspace 2]
    AuthProxy --Proxy--> Workspace3[Workspace 3]
    end
    AuthProxy --OAuth 2--> GitLab
    GitLab --Redirect--> AuthProxy
    AuthProxy --Authz API--> GitLab
```

### Advantages

1. Single instance of proxy, and therefore it is easy to manage and get metrics from.
2. Easy to upgrade as a single instance exists - workspaces do not need to be restarted.

### Disadvantages

1. Single point of failure
2. It will have to scale with traffic
3. New component (other than the GitLab Agent) that would have to be deployed in the Kubernetes cluster by the customer
4. Does need Kubernetes privileges to list service resources.


## Option 2: Sidecar Proxy

In this deployment mechanism, a sidecar will be injected into each workspace and all traffic to the workspace will flow through the sidecar. The sidecar will only handle the traffic for a single workspace. The sidecar can communicate with the workspace over the loopback interface (localhost) since the two share a network namespace.

```mermaid
flowchart TB
    Developer --> Ingress
    subgraph WorkspaceCluster[Workspace Cluster]
        Ingress --> Workspace1Proxy
        Ingress --> Workspace2Proxy
        Ingress --> Workspace3Proxy
        subgraph workspace1[Workspace 1]
            Workspace1Proxy[Workspace Proxy] --Proxy--> Workspace1[Workspace 1]
        end
        subgraph workspace2[Workspace 2]
            Workspace2Proxy[Workspace Proxy] --Proxy--> Workspace2[Workspace 2]
        end
        subgraph workspace3[Workspace 3]
            Workspace3Proxy[Workspace Proxy] --Proxy--> Workspace3[Workspace 3]
        end
    end

    Workspace3Proxy --OAuth 2--> GitLab
    GitLab --Redirect--> Workspace3Proxy
    Workspace3Proxy --Authz API--> GitLab
```


### Advantages

1. Does not need to handle a large volume of traffic
2. If a sidecar stops working it does not hamper the working of other workspaces

### Disadvantages

1. Slight waste of resources as a sidecar will have to be deployed for every workspace
2. Pods might take slightly longer to come up because of the additional proxy element

## Other Options Considered

### Using Auth Annotations in the ingress resource

Another option we considered was using Auth annotations on the ingress resource to allow ingress controllers to delegate authentication and authorization to a separate process. However, we've eliminated that option for now as these annotations are not standard (i.e. not part of the [ingress spec](https://kubernetes.io/docs/concepts/services-networking/ingress/)) and may not be supported across different ingress controllers. We would need to document the process to setup our Auth provider for each of the ingress controllers. However, if they do become a part of the new [Gateway API](https://gateway-api.sigs.k8s.io/concepts/security-model/), we will offer it as an option in the future.

e.g. of auth annotations for nginx ingress:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    nginx.ingress.kubernetes.io/auth-url: "https://$host/oauth2/auth"
    nginx.ingress.kubernetes.io/auth-signin: "https://$host/oauth2/start?rd=$escaped_request_uri"
  name: external-auth-oauth2
  namespace: kube-system
spec:
  ingressClassName: nginx
  rules:
  - host: __INGRESS_HOST__
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: kubernetes-dashboard
            port:
              number: 80
```

e.g. of auth annotations for traefik ingress:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: whoami
  labels:
    app: whoami
  annotations:
    kubernetes.io/ingress.class: traefik
    ingress.kubernetes.io/auth-type: forward
    ingress.kubernetes.io/auth-url: http://traefik-forward-auth:4181
    ingress.kubernetes.io/auth-response-headers: X-Forwarded-User
spec:
  ingressClassName: traefik
  rules:
  - host: whoami.example.com
    http:
      paths:
      - backend:
          serviceName: whoami
          servicePort: http
```

e.g. for Google Cloud Load Balancer using IAP.

```yaml
apiVersion: cloud.google.com/v1
kind: BackendConfig
metadata:
  name: config-default
  namespace: my-namespace
spec:
  iap:
    enabled: true
    oauthclientCredentials:
      secretName: my-secret
```
