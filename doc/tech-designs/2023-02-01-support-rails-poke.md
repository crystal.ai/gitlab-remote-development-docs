# Support Rails poke feature
___

[[_TOC_]]

# Requirements

- An endpoint on KAS that initiates a desired action on the leader node of the agentk.

# Conclusion [NOT IMPLEMENTED]

- Among the presented solutions, solution 7 was the preferred choice on account of simplicity of implementation and because it doesnt merit introducing significant changes to KAS/agentk internals
- However, after a final round of discussions, the final decision is that there is no strong need to implement this feature
- The primary use case for having the poke feature is to be able to trigger a full resync on demand and after a deeper discussion, it seems that there are very few / rare cases that would merit such a capability, especially with the complex nature of implementation required
- Additionally, there isnt a strong enough need to have the resync be carried out **on-demand or immediately** either ; an eventual reconciliation of state would suffice for most cases and it could be simply achieved through full resync that is carried out periodically (perhaps with a much longer interval between cycles)
- While there is no clear usecase as of now, there may be a need in the future to revisit the ideas discussed in this document as they lay out a summary of the changes required to implement an end-to-end flow from Rails → remote dev module

# Solution Overview

There are 2 big parts of the problem that need to be tackled
- KAS -> agentk request path
- Event publication within agentk

## KAS -> agentK request path

### Ideal Scenario
```mermaid
graph TB
  ror -- 1. probe grpc --> kas
  kas -- 2. gRPC --> grpc_handle_lead
  kas -. gRPC -.-> grpc_handle_replica

  subgraph "GitLab"
    kas[kas]
    ror[RoR]
  end

  subgraph "Kubernetes cluster"
      grpc_handle_lead --> probe_module
      probe_module -- 3. publish event --> remote_dev
    subgraph "agentk leader"
        grpc_handle_lead["gRPC handler"]
        probe_module["probe module"]
        remote_dev["Remote Dev module"]
    end
    subgraph "agentk replica"
      grpc_handle_replica["gRPC handler"]
    end
  end
```
To clearly explain the process:

- KAS provides a generic gRPC endpoint for receiving probe requests.
- When the probe endpoint is invoked by RoR, KAS aims to identify the current leader among the agent replicas.
- The leader agentk instance executes an event handler that responds to the probe request and forwards it to the appropriate module (in this case, the remote device module).
### Problem with this approach

- KAS cannot determine if the instance it is communicating with is a leader or not, as each replica establishes a gRPC connection with KAS.
- Additionally, KAS does not have any means of determining if the downstream agentk node is a leader or not.
- When KAS receives an agent-bound request, it searches through all connected agents and selects the first one that can fulfill the request.
- Given that the remote device module is intended to be leader-exclusive, it is necessary to find a way to guarantee that the request reaches the leader instance.

### Possible solutions to work around this

Below are all the solutions I looked into to see if they could be a good fit. I've included options that I eventually considered unfeasible in case someone knows a way to get them to work.

##### Solution 1: Probe broadcast

**Overview**

Expose probe endpoint on each agentk replica. However, the probe module will have a noop behavior when its not running on the leader.

When KAS receives a probe request from RoR, it broadcasts probe requests to all the replicas (possibly by injecting some metadata into the outgoing context that identifies it as a broadcast request)

**Pros**
- Perhaps the simplest to implement
- While not as performant, may not be so much of an issue if most agentk deployments dont have many replicas

**Cons**
- If agentk deployments tend to have many replicas, then this may not be the most optimal solution

```mermaid
graph TB
  kas -- 2. grpc --> grpc_handle_lead
  kas -- 3. grpc --> grpc_handle_replica
  ror -- 1. grpc probe --> kas

  subgraph "GitLab"
    kas[kas]
    ror[RoR]
  end

  subgraph "Kubernetes cluster"
    grpc_handle_lead -.-> probe_module_lead
    probe_module_lead -. publish event -.-> remote_dev_lead
    subgraph "agentk leader"
      grpc_handle_lead["gRPC handler"]
      probe_module_lead["probe module"]
      remote_dev_lead["Remote Dev module"]
    end
    grpc_handle_replica -.-> probe_module_replica
    subgraph "agentk replica"
      grpc_handle_replica["gRPC handler"]
      probe_module_replica["probe module\n(noop when not leader)"]
    end
  end
```

##### Solution 2: signalling via 3rd party message queue that agentk replicas coordinate with

**Overview**
- To facilitate communication between leader and non-leader replicas, we will implement an external message queue.
- All agentk instances will be configured to handle probe requests and forward them to the message queue.
- A module exclusive to the leader will act as the subscriber of the published events.
- Upon receipt of an event, it will be passed internally to the relevant modules that have subscribed to it.

**Pros**
- Can be implemented without touching the connection mgmt internals of the module so any bugs introduced will be limited to remote dev module

**Cons**
- Introduces another moving part which introduces complexity in the deployment plan while adding another unit that must be monitored / maintained

**when gRPC is invoked on the leader agentk**
```mermaid
graph TB
%%    class selected stroke:#ff3,stroke-width:4px,color:red;
%%linkStyle default stroke:#ff3,stroke-width:4px,color:red;

  kas == 2. grpc ==> grpc_handle_lead
  kas -.-> grpc_handle_replica
  ror == 1. grpc probe ==> kas

  subgraph "GitLab"
    kas[kas]
    ror[RoR]
  end

  subgraph "Kubernetes cluster"
    grpc_handle_lead ==> probe_module_lead

    mq["message queue"]
    probe_module_replica -.-> mq
    probe_module_lead == 3. publish event==> mq
    mq == 4. consume event ==> event_consumer_lead
    mq -.-x event_consumer_replica

    event_consumer_lead ==> remote_dev_lead
    event_consumer_replica -.-> remote_dev_replica
    subgraph "agentk leader"
      grpc_handle_lead["gRPC handler"]
      probe_module_lead["probe module"]
      event_consumer_lead["event consumer"]
      remote_dev_lead["Remote Dev module"]
    end
    grpc_handle_replica -.-> probe_module_replica
    subgraph "agentk replica"
      grpc_handle_replica["gRPC handler"]
      event_consumer_replica["event consumer (disabled on non-leader)"]
      probe_module_replica["probe module"]
      remote_dev_replica["Remote Dev module"]
    end

  end
```

**when gRPC is invoked on a non-leader agentk instance**
```mermaid
graph TB

  kas == 2. grpc ==> grpc_handle_replica
  kas -.-> grpc_handle_lead
  ror == 1. grpc probe ==> kas

  subgraph "GitLab"
    kas[kas]
    ror[RoR]
  end

  subgraph "Kubernetes cluster"
    grpc_handle_lead -.-> probe_module_lead

    mq["message queue"]
    probe_module_replica == 3. publish event ==> mq
    probe_module_lead -.-> mq
    mq == 4. consume event ==> event_consumer_lead
    mq -.-x event_consumer_replica

    event_consumer_lead ==> remote_dev_lead
    event_consumer_replica -.-> remote_dev_replica
    subgraph "agentk leader"
      grpc_handle_lead["gRPC handler"]
      probe_module_lead["probe module"]
      event_consumer_lead["event consumer"]
      remote_dev_lead["Remote Dev module"]
    end
    grpc_handle_replica ==> probe_module_replica
    subgraph "agentk replica"
      grpc_handle_replica["gRPC handler"]
      event_consumer_replica["event consumer (disabled on non-leader)"]
      probe_module_replica["probe module"]
      remote_dev_replica["Remote Dev module"]
    end
  end
```

##### Solution 3: Implement probe module as a gRPC service that be dynamically registered / unregistered on the leader module

**Overview**
- **Background**:
    - Modules on agentk that communicate with KAS through gRPC register the module's business logic as gRPC services.
    - When agentk establishes a new connection with KAS, it collects and transmits information about all the services registered by the modules and shares it with KAS.
    - KAS stores this information internally and uses it to route agentk-bound rpc calls to the first available agentk connection.
- In spite of some digging, I couldnt really find an easy API call that allows services to unregister handlers on-the-fly
- In theory, some workarounds can be added but it would make for a very complex implementation
    - after a leader re-election, an outgoing leader can create fresh connections (without the leader-specific services) and in incoming-leader can do something similar with leader-specific services registered onto a new connection pool
    - but this will involve a lot of tinkering with low-level connection mgmt components of agentk and im not sure if this is the easiest to implement

**Pros**
- Achieves the requirements..

**Cons**
- Unfeasible because it would involve touching a lot of low-level connection management logic. Increases the surface area for new bugs which may affect existing stable functionality
- Will require thorough regression testing of existing functionality

##### Solution 4: agentk informs KAS of who is the active leader and KAS will use this information when choosing between replicas

**Overview**
- Changes required in agentk: Implement a leader-only module notifies KAS about the latest leader
- Changes required in KAS:
    - Update the tunnel registry logic to keep track of the latest leader
    - Support additional metadata in agentk-bound rpc requests to indicate if it must only reach the leader
    - Add logic at the right layer to handle this metadata

**Pros**
- Doesn't touch the low-level logic as much as the other solutions so not the worst option on the table

**Cons**
- KAS is now leader-aware so that adds to existing complexity of interactions between KAS and agentk which is better to avoid if possible

##### Solution 5: agentk creates separate gRPC connection pool for leader-only module with separate services registered onto them

**Overview**
- On the agentk, a new module will be introduced, reusing the connection management logic from the reverse tunneling module (which may require some code restructuring). This will be a leader-exclusive module, so if the leader changes, the connections will be terminated. This module will be utilized by services that wish to provide services on the leader agent only.
- The underlying concept is that when a new connection (managed by this new module) is established between agentk and KAS, the descriptors shared with KAS will correspond to leader-only services.
- Thus, when KAS receives a probe request, it will only be able to identify leader-only connections that can handle those requests.

**Pros**
- Hopefully it can be implemented in a manner that will minimize impact to existing functionality

**Cons**
- A somewhat complex solution that relies on duplicating some of the low-level connection mgmt logic

##### Solution 6: (Gitops-like flow) agentk establishes long-polling connections with a gRPC service on KAS

**Overview**
- In the earlier solutions, it was proposed to host the probe gRPC service on agentk leader instance, with KAS acting as a client that relays probe requests from Rails to agentk
- In this solution however, the core difference is that agentk will not serve any probe endpoints. Instead leader agentk will act exclusively like a gRPC client that will establish long-running connection with a gRPC service hosted on KAS, waiting for any probe payload to be relayed by KAS
- Note: only leader instance of agentk will establish these connections with KAS. An outgoing leader will terminate these connections through context cancellation
- KAS will now be expected to serve 2 gRPC endpoints
  - one for Rails to make probe requests
  - one for leader agentk instance to connect and receive probe requests from KAS

**Things to resolve**
- While this solution greatly reduces the complexity of changes required on agentk side, there is an additional unknown of how we manage KAS -> KAS -> agentk communication
- To illustrate this problem, lets consider the following scenario. Say a probe request from Rails is received by KAS instance 1. However, the leader agentk instance is connected to KAS instance 2.

```mermaid
graph TD

  subgraph "GitLab"
    ror[RoR]

    subgraph "KAS"
      kas1["kas instance 1"]
      kas2["kas instance 2"]
    end
  end
  
    ror == 1. grpc call==> kas1
    kas2 == 2. wait for probe work <==> probe_module_lead

  subgraph "Kubernetes cluster"
      
    subgraph "agentk leader"
      probe_module_lead["probe module"]
      remote_dev_lead["Remote Dev module"]
      
      probe_module_lead ==> remote_dev_lead
    end
    
    subgraph "agentk replica"
      probe_module_replica["probe module (inactive)"]
      remote_dev_replica["Remote Dev module (inactive)"]
      
      probe_module_replica ==> remote_dev_replica
    end
  end
```
- Currently, KAS handles this problem by internally routing requests from KAS 1 to KAS 2 by querying redis for KAS instances that are connected to the target agentk
- However from what I can see, once the request has reached KAS instance 2, it will only relay requests to the agentk **iff** the agentk is connected using the reverse tunnel approach (i.e. as an end gRPC service) and not a gRPC client
- It seems like this is not an issue with the existing Gitops module because for Gitops, entire workflow is not initiated by Rails. As such, there is never a need to solve the `origin KAS -> target KAS <- agentK` problem because the business logic is not triggered by Rails invoking KAS
- This is also not a problem with the other proposed solutions because they involve supporting the feature on agentk via its reverse-tunneling module and the existing machinery on KAS supports that out-of-the-box
- From what I can see, there are 2 ways to approach this problem
  - **Use redis as a pub/sub broker within KAS system**: 
    - As stated, we can use Redis to broker probe request between the origin KAS instance and target KAS instance
    - Redis is already a dependency in KAS so we will not be introducing anything new
    - However, redis is not being used as a pub/sub broker yet so it may raise scalability issues depending on the probe request traffic
  - **Modify KAS internal routing logic to support forwarding messages to agentk that is not connecting via reverse-tunnel approach**
    - While it doesnt use redis for pub/sub, it will be required for KAS -> KAS routing
    - KAS -> agentk routing logic will need to be modified to check if the target gRPC can be handled by a KAS service registered on KAS instance itself (seems a bit counterintuitive)


##### Solution 7: (Gitops-like flow) KAS triggers / coordinates remote dev module logic on agentk

This solution is similar to solution 6, in that that agentk will not serve any probe endpoints. Instead leader agentk will act exclusively like a gRPC client that will establish long-running connection with a gRPC service hosted on KAS

However, the existing looping logic within remote_dev module on agentk will need to reworked. For reference, remote dev module on agentk executes the following logic sequentially in a loop to sync system state across the cluster and Rails:
  - Gather latest workspace data across the cluster
  - Call Rails API with the latest info. In the API response, Rails will populate the work that agentk needs to do
  - Apply the updates received from Rails to the cluster

This solution requires splitting responsibilities across KAS and agent in the following manner
- **On KAS**:
  - Once an agent has established a connection with KAS, begin a loop on KAS that periodically sends triggers to agentk. It doesn’t check for work on Rails or anything of that sort
  - Probe module will reside on KAS only. It will publish events that the loop(created in the previous step) will subscribe to. 
  - On a probe event, the next interval wait for the loop will be skipped, allowing KAS to *publish a new trigger to agentk immediately 
- **On agentk** 
  - Unlike the existing implementation, agentk will not manage the looping behavior. Instead it will establish a stream with KAS and keep listening for triggers. 
  - Upon receiving a trigger message, agentk will carry out the same sequence of steps that it currently does
  - The only difference is that agentk is no longer in control of polling while still being in charge of the steps of syncing

**Pros / Cons**
- It doesn’t involve making too many changes to the remote dev module
- We avoid the complexity of dealing with too many internals (such as leader re-election etc) since the probe module will now reside on KAS alone
- The complete polling flow now spans agentk and KAS vs just agentk which is the case atm. The most obvious impact will be on network traffic but probably not by much?


## Event publication to remote dev module

- Once we can ensure that the request lands at the gRPC endpoint in the correct node (whether its agentk or KAS, where the remote dev module is running), we need only relay it to the right module
- For solutions 1-6, this will need to be implemented in agentk. For solution 7, this logic will need to reside within KAS
- There is an additional need to implement this in a generic manner so that it can be seamlessly reused in future features
- Create an event store `EventStore` entity that will be responsible for consuming events from the probe endpoint and publishing them over to the target module via channels.
```go
type EventStore interface {
    PushMessage(ctx Context, moduleName string, event Event)
    SubscribeToMessage(ctx Context, moduleName string) <-chan Event
}
```
- Event must have the following metadata: module_name[String]; event_id[int]; event_payload[[]bytes]
```protobuf
message Event {
  required string module = 1;
  required int32 event_type = 2;
  repeated bytes event_payload = 3;
}
```
- Parsing event specific info is the responsibility of the module that owns it. module name will be used to route the event to the appropiate module.
- A module may or may not wish to subscribe to its events. Similar to how a module declares its intent to be leader-only by conforming to `LeaderModule` interface, a module may indicate its intent to subscribe to events by conforming to a `EventSubscriberModule`
```go
type EventSubscriberModule interface {
	Module

	SetEventSubscription(subscription <-chan Event)
}
```
- During module initialization, each module will be check whether it intends to become a subscriber. If yes, a channel will be created that listens to events for the module and the setter `SetEventSubscription` will be called
- The event stream should ideally not be blocked due to a slow consumer.

# References
- [Relevant issue](https://gitlab.com/gitlab-org/gitlab/-/issues/387090)
- [Merge Request (for going through discussions)](https://gitlab.com/gitlab-org/remote-development/gitlab-remote-development-docs/-/merge_requests/13)

