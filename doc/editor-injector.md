# Editor injector

The editor injector is a container image that contains our VS Code server. The editor injector contains scripts for copying this server into a workspace and starting the server.

## Architecture

![Injector architecture](../assets/injector-architecture.png)

The injector packages both the WebUI and the Extension Host (VS Code backend). [For now](https://gitlab.com/gitlab-org/gitlab/-/issues/393006), we also package the WebUI in the workspace. That means that the VS Code editor can be used two ways:

- Access the Workspace URL **directly** and use the bundled WebUI
- Access the Workspace through **WebIDE** (Ignore the bundled WebUI)

## Usage

The following steps are the easiest way to get the Injector working locally:

### Start the workspace from CI Image in your local Rancher

_Not all containers can be run locally, please see the [CI-built images](#ci-built-images) section for details._

1. Create a workspace with WebIDE server injected:

   ```sh
   kubectl apply -f assets/editor-injector-local-development.yaml
   ```

1. Get the URL of the workspace:

  ```sh
  kubectl -n default get service editor-injector-local-development -o yaml |
    grep "remotedevelopment.gitlab/workspace-domain-template:" |
    cut -d':' -f2 | xargs | sed -e 's/{{.port}}/8000/g'
  ```

Then you can access the VS Code in the workspace:

- Directly
  - Add `folder` (optional) get parameters.
    - example: `https://8000-editor-injector-local-development.workspaces.localdev.me/?folder=%2Fprojects%2Fmicroservices-demo-frontend`
- WebIDE

  1. If your workspace uses self-signed certificates, first visit the workspace directly and accept the self-signed certificates in your browser.
  2. Go to [example WebIDE app](https://gitlab-org.gitlab.io/gitlab-web-ide/) or [build the WebIDE locally](https://gitlab.com/gitlab-org/gitlab-web-ide/-/blob/main/docs/dev/development_environment_setup.md#setup).

     - select "Remote Development" in the dropdown
     - add the hostname (not the full `http://`) URL as the "Remote Authority"
       - e.g. `8000-editor-injector-local-development.workspaces.localdev.me`
     - set host path to `/`
     - Set connection token as `password`

  3. Click "Start GitLab WebIDE" button.

## Building and using injector locally

Editor injector is built from our fork of VS Code [`gitlab-web-ide-vscode-fork`](https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork).

To build the editor injector locally, follow these steps:

1. Clone the repository `git clone git@gitlab.com:gitlab-org/gitlab-web-ide-vscode-fork.git`
1. Install dependencies `yarn`
1. Run the `./scripts/gl/gl_build_editor_injector_locally.sh` script.
1. The script builds an image and prints out its name. Use this name for local development.

You've now built the image as `editor-injector:

1. Change the image name in `assets/editor-injector-local-development.yaml` Deployment init container `editor-injector` to the image you generated in previous section. (e.g. `editor-injector:1.75.1-1.0.0-dev-20230221085045`)
1. Change the image name in `assets/editor-injector-local-development.yaml` Deployment container `tooling-container` from `quay.io/mloriedo/universal-developer-image:ubi8-dw-demo` to `ubuntu`.

### ARM vs AMD problem locally

We only build editor-injector for AMD platform at the moment (see [Build editor-injector for ARM platform](https://gitlab.com/gitlab-org/gitlab/-/issues/392693)). This means that when downloading the image from our container registry (e.g. `registry.gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/web-ide-injector:1.73.1-1.0.0-dev-20230210132742`), we only download the AMD version (the only available).

That works well with the `quay.io/mloriedo/universal-developer-image:ubi8-dw-demo` image defined in `assets/editor-injector-local-development.yaml`, because that image is also AMD (and thanks to some emulation can be run on our M1 macs).

When you build the images locally with the `gl_build_editor_injector_locally.sh` script, you build both `amd64` and `arm64` versions. Then you can't use the `ubi8-dw-demo` image. Kubernetes will use the ARM injector which copies the ARM VS Code and the `ubi8-dw-demo` will fail to run it because it is an AMD image.

To run a locally-built editor injector do _either_ of the following steps:

- change the nerdctl platform in `gl_build_editor_injector_locally.sh` to only build amd64 images
- change the main `tooling container` in `assets/editor-injector-local-development.yaml` from `ubi8-dw-demo` to `ubuntu` (or any other image that supports arm64).

### Local testing without workspace

The following steps are useful when you want to use/test the injector without starting a DevWorkspace

1. create a volume `nerdctl volume create test`
1. copy server to a volume `nerdctl run -it -v test:/volume --namespace k8s.io --pull=never -e "EDITOR_VOLUME_DIR=/volume" editor-injector:1.75.1-1.0.0-dev-20230221085045`. Replace the image version with the version of the image you've built.
1. see what's been copied on the volume `nerdctl run -it -v test:/volume --namespace k8s.io --pull=never --entrypoint /usr/bin/bash ubuntu`
1. run the server `nerdctl run -it -v test:/volume -e "EDITOR_VOLUME_DIR=/volume" -e "TOKEN=password" -p 8000:8000 --namespace k8s.io --pull=never --entrypoint /volume/start_server.sh ubuntu` (this should work, but I haven't connected the WebIDE to the server)

## CI-built images

The images are built in the [`gitlab-web-ide-vscode-fork`](https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork) project by the build-editor-injector CI job.

The images are stored in the GitLab container registry as the [`web-ide-injector`](https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/container_registry/3918839) container image.

When used with WebIDE, we should match the [VS Code version specified in WebIDE](https://gitlab.com/gitlab-org/gitlab-web-ide/blob/9a82ee837774e17f73de9f3d268911e8e30e62ea/packages/vscode-build/vscode_version.json#L4). Matching the version is at the moment manual and cumbersome process. The issue to fix this process is [Design mechanism to match versions of VS Code server and WebIDE](https://gitlab.com/gitlab-org/gitlab/-/issues/373669).
