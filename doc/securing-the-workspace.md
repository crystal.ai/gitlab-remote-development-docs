# Securing the Workspace

Apart from [authenticaion and authorization of user traffic](./auth.md),
there are other ways in which we can secure the entire workspace environment.

#### Add security context to Pod

A [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)
defines privilege and access control settings for a Pod or Container.
There are multiple settings that we should explore and work on.

#### Harden all container images being used

We should aim to use container images for all the components that we build/use so that
the attack surface and exposure to bugs and vulnerabilities is reduced.
We can explore [`distroless`](https://github.com/GoogleContainerTools/distroless) in this regard.

"Distroless" images contain only your application and its runtime dependencies.
They do not contain package managers, shells or any other programs you would expect to
find in a standard Linux distribution.

Additional benefit is that Distroless images are very small. Thus, it would help in
pulling images faster, reducing bandwidth consumption and saving network costs.

Since distroless images do not include a shell or any debugging utilities,
it's difficult to troubleshoot distroless images using `kubectl exec` alone.

#### Isolate each workspace with a Kubernetes Namespace

Each workspace should have a unique namespace in Kubernetes. This allows us to isolate
workspaces more easily in Kubernetes. Any user specific resources(e.g. user's workspace preferences,
user secrets, etc.) can be created for each workspace. It also allows us to add any other policies like
[Network Policies](https://kubernetes.io/docs/concepts/services-networking/network-policies/)
more easily.

#### Block all traffic to the workspace by default

Currently, all traffic is allowed to enter the workspace. We should tackle this by
configuring rules to deny all traffic and only allow the ones which we desire using
Kubernetes Network Policies.

Kubernetes [Network Policies](https://kubernetes.io/docs/concepts/services-networking/network-policies/)
are an application-centric construct which allow you to specify how a pod
is allowed to communicate with various network entities over the network.

The entities that a Pod can communicate with are identified through a combination of the
following 3 identifiers:

1. Other pods that are allowed (exception: a pod cannot block access to itself)
1. Namespaces that are allowed
1. IP blocks (exception: traffic to and from the node where a Pod is running
is always allowed, regardless of the IP address of the Pod or the node)

However, Network policies are implemented by the Kubernetes network plugin.
To use network policies, you must be using a networking solution which supports NetworkPolicy.
Creating a NetworkPolicy resource without a controller that implements it will have no effect.

#### Explore other ideas

* [Pod Security Standards](https://kubernetes.io/docs/concepts/security/pod-security-standards/)
