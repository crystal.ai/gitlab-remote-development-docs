# Feature Implementation Details


[[_TOC_]]

NOTE: This document is currently in draft mode and is being actively developed.

See [Architecture](./architecture.md) before reading this document for a better understanding.

This document is mainly focussed on adding the implementation details of features that will be part of the minimal viable release.

## Features for [Viable maturity](https://about.gitlab.com/direction/maturity/)

### Dynamically injecting editor binaries into the workspace

The user can choose any container image to be run within the workspace. These images are not expected to have the editor binaries available within them. They should be dynamically injected while provisioning the workspace.

Implementation
- Create a multi-architecture container image per-editor containing the editor's self-contained binary which can be copied to any arbitrary location
- Run an `init container` as part of workspace provisioning to copy editor binary into a volume mounted to the init container
- Ensure that the same volume is mounted to the main container of the workspace
- Modify the main container's command/arg to start the editor

Open questions
- How to we accommodate container images whose entrypoint and command have been override during runtime?
    - Use `docker inspect` to get the container image's entrypoint and command
    - Calculate the final entrypoint and command from the above information and devfile's `pod-overrides` and `container-overrides`
    - Modify the main container's command/arg to start the editor and then perform its own action
- How to dynamically inject health check of the editor into the health check of the main container of the workspace?
    - We can add an artificial limit that we will only allow `exec` health checks and then during workspace provisioning, modify the healthcheck command to include editor healthcheck along with the container's defined healthcheck

### Cloning projects

Devfile specification allows cloning of projects from `git` and `zip` sources. It also allows the user to specify multiple projects. As part of workspace provisioning, these projects should be cloned into the workspace.

Implementation
- Create a container image for cloning projects from a git or zip source
- Run an `init container` as part of the workspace provisioning to clone all the projects into a volume mounted to the init container
- Ensure that the same volume is mounted to the all containers of the workspace which have `.components[].conatainer.mountSources` as `true` at the location specified by `.components[].conatainer.sourceMapping` in the devfile
- To ensure cloning of both public and private projects, a personal access token is injected into the init container as an environment variable

Open questions
- Should the personal access token be unique for each workspace or each user(refreshed at regular intervals)? What will the lifecycle of the personal access token be? When should they expire? What happens if the workspace is running but the token has expired?
- When the workspace restarts, the projects would already be cloned. Should this `init container` just exit if the projects are already cloned? Or should it fetch the upstream updates?
- We should not expect users to explicitly specify the `projects` in a devfile especially if the workspace is being created from a devfile within a repository. In such a case, we should automatically inject the repository's git location into the `projects` section. How do we ensure that the same project is not specified twice - once in implicitly-added-project and another in a explicitly specified zip source? What happens in case of a name clash?
- Should we provide different modes of cloning projects in later releases? In the initial release, project cloning happens before the main container of the workspace starts. We can also provide an option for cloning the project in the background of the main container of the workspace.

### Injecting user's git credentials and config

The user should be able to push a commit to upstream seamlessly. For this, the git config needs to be set.

Implementation
- Create a personal access token while creating the workspace
- The lifecycle of this token is attached to lifecycle of the workspace
- For initial version, the expiry date of the token will be set to `now + 3 months` with the assumption that the workspace will be terminated within 3 months. However, for future releases, we would have to devise a mechanism to refresh this personal access token if it expires before the workspace is terminated
- This token is used for the initial project cloning and will also be used to configure the user's git credentials and config

Open questions
- What are the security implications of generating a personal access token per workspace? Using oauth2 token is not feasible for initial project cloning because it requires the user to login before we can use/acquire those tokens. The personal access token would be tied to the lifecycle of the workspace. A user can do any kind of action within the workspace e.g. build a docker image and push it to a specific gitlab registry. They would expect it to work according to the permissions attached to their user. Hence, we cannot use project access tokens or group access tokens. Personal access token are attached to a user and would thus inherit the permissions of the user which is similar to what an oauth token is.

### Defining security context for a workspace

[Kubernetes pod specification allows you to specify the user ID, group ID, permissions of files in volumes mounted by the pod, etc](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/). These settings can be specified at both the pod and container level. Thus, they can also be specified in the `pod-overrides` and `container-overrides` attributes of devfile. However, we cannot allow users to specify rules which can lead to privilege escalation. e.g. no container should be run as a root user.

Open question
- What should be the default value for the security context?
- What is the highest security context that we should allow the user to be able to specify in their devfile beyond which we shall treat the devfile as "invalid" due to having very open security context?

### Injecting common environment variables

It would be beneficial to the workspace user to have certain details regarding the workspace be injected into the workspace as environment variables. e.g. workspace name, agent id, username, base url suffix of the workspace, etc.

Open question
- List of variables that should be injected into the workspace
- Should we inject these variables as directly as `env` in a deployment config or should we create a configmap/secret and mount them as environment variables in the deployment config?

### Provisioning dynamic volume for each workspace

Kubernetes has `StorageClass` which abstracts away the complexity of provisioning a volume on-demand on a given provider - aws, gcp, azure, on-prem, etc. While creating a workspace, we will create a persistent volume claim with the default storage class which will automatically provision the volume for us. It would be the responsibility of the kubernetes administrator to ensure that a default storage class is configured for the cluster. For future releases, we can allow an option to specify which storage class the workspace should be provisioned with.

These volumes get mounted to the workspace upon being provisioned and ready. When a workspace is deleted, they will be deleted automatically.

Open questions
- What should the recommended value of `Volume Binding Mode` of the default storage class be? Using the default `Immediate` can lead to some scheduling issues but using the alternative `WaitForFirstConsumer` restricts us from specifying node affinity in pod specification.
- What happens if the volume deletion failed for some reason? How to handle this scenario?

### Providing image pull secrets

Users should be able to specify container images from GitLab registry in their devfile. This implies that the workspace being created should be able to pull from a private GitLab container registry. Kubernetes allows you to create image pull secrets and attach them to a service account or deployment. The workspace being created should have image pull secrets attached to itself so that it can pull from private registry

Open questions
- Are we expecting the kubernetes cluster admin to create a secret with a predefined name in a predefined namespace which we can then reference in our workspace?
- The users might have other private registries they would want to pull from. How do we achieve that?
- Should this replicate what we do with the CI jobs? Each CI job has an ephemeral token generated for the duration of the job. This token then has the same rights as the user who triggered the pipeline. This token can be used to access the GitLab container registry. Can we use the personal access token of the user that is tied to a given workspace? This will also ensure that the user is only able to pull images that they have access to.
- Since we can attach multiple pull secrets to a given service account/deployment, should we attach the user's personal access token for pulling container images from GitLab and also attach the pre-defined secret which the cluster admin has created which contains pull secrets for other registries. This way we enforce granular permissions for images in GitLab registry but also allow pulling from other registries through a global user(where granular permissions are not yet there).

### TLS Certificate Generation

Devfile specification allows exposing multiple endpoints. We would want all these endpoints to be accessible over TLS. Since the ingress point is the ingress-nginx running in the Kubernetes cluster, we need the TLS certificates to be available in the cluster so that ingress-nginx can decrypt the traffic and forward it to the container.

Implementation
- Group administrator configures an agent
- Group administrator create a public-private key to be used to encrypting/decrypting TLS certificates to be stored in Postgres
- Group administrator provides public key to GitLab instance (by adding it in agent's `remote_development` module configuration?) and creates a secret in kubernetes using the private key
- GitLab issues a wildcard certificate from Let's Encrypt, encrypts it using the public key in the agent's configuration and stores it in Postgres
- Agentk deployed inside kubernetes, polls Rails for TLS certificate. Once received, decrypts the encrypted TLS certificate, and creates the secret in kubernetes for all Ingress objects to use it
- On agentk restart, it repeats the above thing again (except Rails would respond instantly since the certificate is already provisioned and stored in Postgres)

Open questions
- On what domain will a workspace be accessible? Will it be a subdomain of `gitlab.com` or a different domain like we do for GitLab Pages i.e. `gitlab.io`?
- Is it a security issue to create wildcard certificates and deploy them as secrets in Kubernetes?
- What happens if Let's Encrypt is down during certificate generation? Will the agent keep waiting(blocking) until the certificate is available?
- Should we persist the TLS certificate in Postgres?
    - Reasons for persisting the certificate in Postgres are
        - Customers shouldn't have to delete and create a new agent is someone has made some inadvertent changes to the TLS secret
        - We can run automated jobs in GitLab to renew the certificate and the "poke" agent to update its TLS certificate secret
- Alternative solution - administrator can provide the secrets in the agent config (reference to a kubernetes secret - not the actual value) when are used by the Ingress resource for TLS certificate. The responsibility of the TLS certificate generation would be on the administrator.

### Exposing endpoints for public/internal/none access

Devfile specification allows specifying the visibility of endpoints as public/internal/none. However, the specification isn't crystal clear on what does public/internal/none mean. Regardless, each endpoint should be protected by correct authentication and authorization, the details of which are being tracked in [Securing the Workspace](./securing-the-workspace.md). We need to get clarification from the devfile team around this and define our authentication and authorization strategy for each exposure.

Open questions
- How to handle various options to handle for endpoint exposure e.g. `secure` which puts the endpoint behind a JWT proxy?
- What does public/internal/none exposure mean in the context of GitLab's Remote Development?

### Handling various failure modes

The workspace can fail for various reasons - not being able to pull container image, container crashing, volume mount error, failed scheduling, etc. We can detect the existence of some failure if the deployment is not in a ready status after x seconds. However, it is currently not always easy to get the error in an efficient manner.

We should compile a list of failure scenarios and add e2e tests for these scenarios.

The list of possible failure scenarios (research and add more to the list)
- Kubernetes/Agent/KAS/Rails/Postgres is not available
- Unable to pull container image due to insufficient access, long pull time, etc.
- Unable to run container image due to mismatching architecture, unstable code, etc.
- Unable to schedule pods
- Unable to provision a volume
- Unable to delete a volume
- Unable to mount a volume

**We need to do more research here**

Open questions
- What happens in each scenario and how would we handle them?
- Useful reference - https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#failed-deployment

### Workspace accepts traffic once all components are ready

Workspace should only accept traffic once the pod is health, deployment is ready, service has active endpoints, ingress is configured with correct TLS certificates, etc.

**We need to do more research here**

Open questions
- What should the UX look like until the workspace is getting ready?
- How do we effectively detect if the workspace is ready or not? Is detecting the ready status of the deployment enough?

Possible solution #1
- Do not show the URL on the dashboard/API unless the workspace is ready in the database?
- However, this still does not cover the scenario where workspace has been marked ready because the deployment is ready but the ingress isn't.

Possible solution #2
- Set the default backend of ingress-nginx to a custom service
    - Reference/example - https://github.com/kubernetes/ingress-nginx/tree/main/images/custom-error-pages
    - Let's call this the ingress-error-handling-service
    - Maybe this service can be part of agentk itself? Maybe a separate module being run on non-leader nodes?
    - Enable custom error handling on ingress-nginx
- ingress-error-handling-service will check if the workspace exists( maybe through k8s API? or some cache it received from rails?)
    - If workspace is not yet ready, serve a custom page saying so and do an automatic reload every 5 seconds
    - If no workspace exists, send 404(or anything that we want)
- This also helps us to have a better control over all the possible errors that can occur from outside/within the workspace through the ingress controller and how we want to show the errors to the user
- Setting a default backend sounds like an option that should be available in all ingress controllers. That way, this logic isn't specific to ingress-nginx controller (I would hope so)

# Features for future releases to achieve [Complete/Lovable maturity](https://about.gitlab.com/direction/maturity/)

### SSH into workspaces

User should be able to SSH into their workspaces through the command line.

Implementation
- Along with ingress-nginx accepting HTTP/HTTPS traffic, add another ingress with accepts TCP traffic on a particular subdomain e.g. `<workspace_name>-<port>.ssh.<agent_id>.example.com`
- This ingress will detect the workspace the traffic is to be routed to from the the incoming URL
- While creating a workspace, create relevant Service and Ingress objects

Open questions
- Instead of generating a new set of public private keys, can we use the public keys we've got for Git access and accept them for SSH access so that the user has a seamless experience?

### Idle workspace timeout

If a workspace is not active for `x` amount of time, it should be automatically stopped to save costs to the organization. 

Open questions
- At what level should the controls for setting this time limit should be at? At the group and project level? At the user level?

### Defining secrets in GitLab to inject in a workspace

Users should be able to define secrets to be injected into a workspace at group, project, user level.

### Alerting when a mounted configmap/secret changes value

If any value if a mounted configmap/secret changes, a pop-up in the IDE should alert the user and provide options for the changes to take effect.
