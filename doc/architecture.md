# Architecture

[[_TOC_]]

NOTE: This document is currently in draft mode and is being actively developed.

This is not intended to be the final architecture - it is a minimal viable architecture
to meet the current requirements.

There will be a number of iterations on this architecture as we learn more about
requirements such as performance and scalability.

## GitLab Agent for Kubernetes Remote Development Module Architecture

The overall goal of this architecture is to ensure that the **_desired state_** of all
Remote Development workspaces is represented by the **_actual state_** of the
workspaces running in the Kubernetes clusters.

This is accomplished as follows

1. The desired state of the workspaces is obtained from user actions in the GitLab UI or API and
   persisted in the Rails database
2. There is a polling loop which:
   1. Retrieves the actual state of the workspaces from the Kubernetes clusters and sends it to Rails
      to be persisted
   2. Rails compares the actual state with the desired state and initiates any workspace status actions to
      bring the actual state in line with the desired state.

### Terminology

These are some terms which are used in the following UML diagrams.

**NOTE: Some of these terms or naming conventions may change in the future**

`Rails`:

* This is our Rails monolith, exposing GraphQL and REST APIs and having a connection to the Postgres DB.
* The business logic would reside in the Rails monolith
  * Right now, business logic includes transforming devfile to kubernetes resource, injecting the editor and project cloner inside it, extracting the actual state of the workspace from the information sent by agentk, etc.
  * Eventually, business logic will include configuring resources based on which tier the user belongs to. e.g. If the user is free, maximum allowed CPU/RAM usage is x units.

`Postgres`:

* This is the Postgres database in the Rails monolith, where the state of all the remote development workspaces
  is persisted.

`Gitaly`

* Some data may be retrieved directly from Gitaly, e.g. the contents of the devfile config for a workspace.

`GitLab Agent for Kubernetes` (`GA4K`)

See the following docs for `GA4K`:
* User docs: https://docs.gitlab.com/ee/user/clusters/agent/index.html
* Developer docs: https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/tree/master/doc
* Architecture docs: https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/architecture.md

`GA4K Server` (`kas`)

* The "server" component of `GA4K`. Single service of `kas` runs colocated with Rails (service has one configuration, but can have multiple replicas).

`GA4K Agent` (`agentk`)

* The "agent" component of `GA4K`. Multiple services of `agentk` run on the Kubernetes cluster, and each
  agent service can run multiple pods.

`kas` `remote_development` module

* In the initial architecture, the polling requests to rails will be initiated by `agentk`, and `kas` will
  merely act as a proxy to rails. Therefore, there will be little or no `remote_development`-specific code in `kas`
  in this initial architecture. This may change in the future as part of performance, scalability, or
  other optimizations.

`agentk` `remote_development` module

* It has a regular polling loop.
* It has access to the Kubernetes API, and listens for changes related to workspaces.
* It makes REST POST requests to Rails, by proxying them through `kas`
* On each poll, it sends a POST request to the Rails REST API with data representing the actual state of the workspaces
  in the request body, and receives any new desired state to be applied from Rails in the response body.

`Kubernetes API` (`kubernetes`)

* The standard Kubernetes API server.

### Network Topology Overview

Notes:

- The Kubernetes API is not shown in this diagram, but it is assumed that it is managing the workspaces via the `agentk`.
- The numbers of components in each cluster are arbitrary.

```plantuml
@startuml

node "GitLab Monolith" as gitlab {
  rectangle rails
  database postgres
  rectangle "kas deployment" as kas_deployment {
      collections kas1..kas8
  }
}

cloud cloud

cloud -left- kas1..kas8
kas_deployment -left- rails
rails -left- postgres

node "kubernetes cluster" as kubernetes {
  rectangle "agentk A workspaces" as agentka_workspaces {
      collections workspace2..workspace8
      rectangle workspace1
  }

  rectangle "agentk B workspaces" as agentk2..agentk8_workspaces {
      collections workspace10..workspace16
      rectangle workspace9
  }

  rectangle "agentk deployment A" as agentk_a_deployment {
      rectangle agentk_a_1
  }

  rectangle "agentk deployment B" as agentk_b_deployment {
      collections agentk_b_1..agentk_b_8
  }

  agentk_a_1 -right- agentka_workspaces
  agentk_b_1..agentk_b_8 -right- agentk2..agentk8_workspaces
}

'the following hidden line is a hack to get the diagram to render correctly
agentk_a_1 -[hidden]- agentk_b_1..agentk_b_8
cloud -right- agentk_a_1
cloud -right- agentk_b_1..agentk_b_8


@enduml

```

### Architecture Infrastructure Overview

```plantuml
@startuml
node "Kubernetes" {
  [Ingress Controller] --> [GitLab Workspaces Proxy] : Decrypt Traffic

  note right of "Ingress Controller"
    Customers can choose
    an ingress controller
    of their choice
  end note

  note top of "GitLab Workspaces Proxy"
    Authenticate and
    authorize user traffic
  end note

  [GitLab Workspaces Proxy] ..> [Workspace n] : Forward traffic\nfor workspace n
  [GitLab Workspaces Proxy] ..> [Workspace 2] : Forward traffic\nfor workspace 2
  [GitLab Workspaces Proxy] --> [Workspace 1] : Forward traffic\nfor workspace 1

  [Agentk] .up.> [Workspace n] : Applies kubernetes resources\nfor workspace n
  [Agentk] .up.> [Workspace 2] : Applies kubernetes resources\nfor workspace 2
  [Agentk] .up.> [Workspace 1] : Applies kubernetes resources\nfor workspace 1

  [Agentk] --> [Kubernetes API Server] : Interact and get/apply\nKubernetes resources
}

node "GitLab" {
  [Nginx] --> [GitLab Rails] : Forward
  [GitLab Rails] --> [Postgres] : Access database
  [GitLab Rails] --> [Gitaly] : Fetch files
  [KAS] -up-> [GitLab Rails] : Proxy
}

[Agentk] -up-> [KAS] : Initiate reconciliation loop
"Load Balancer IP" --> [Ingress Controller]
[Browser] --> [Nginx] : Browse GitLab
[Browser] -right-> "Domain IP" : Browse workspace URL
"Domain IP" .right.> "Load Balancer IP"

note top of "Domain IP"
  For local development, workspace URL
  is [workspace-name].workspaces.localdev.me
  which resolves to localhost (127.0.0.1)
end note

note top of "Load Balancer IP"
  For local development,
  it includes all local loopback interfaces
  e.g. 127.0.0.1, 172.16.123.1, 192.168.0.1, etc.
end note

@enduml
```

### Workspace management APIs and messaging

All requests/responses between agentk and rails are synchronous.

#### Types of messages

Agentk can send different types of messages to rails to capture/receive different information. Depending upon what type of message agentk send, rails will respond accordingly.

Different types of messages are -

- `prerequisites` (yet to be implemented) - This is the first message agentk sends to rails right after agent starts/restarts/leader-election.
  - `agentk`
    - Fetch kubernetes resources that are required to be available in the kubernetes cluster.
  - `rails`
    - Send the kubernetes manifests for `gitlab-workspaces-proxy`  that need to be available in the kubernetes cluster.
- `workspace_updates` - Messages sent to rails with type `workspace_updates` must further set an `update_type` field with the following possible values: `full` and `partial`. The payload schema remains the same for both update types.
  - `full`
    - `agentk`
      - Send the current state of all the workspaces in the kubernetes cluster managed by the agent.
      - To keep things consistent between agentk and rails, agentk will send this message every time agent undergoes a full sync cycle that occurs 
        - when an agent starts or restarts
        - after a leader-election
        - periodically, as set using the `full_sync_interval` configuration (default: once every hour)
        - whenever the agent configuration is updated
    - `rails`
      - Update postgres with the current state and respond with all the workspacess managed by the agent and their last resource version that rails has persisted in postgres. 
      - Returning the persisted resource version back to agentk gives it a confirmation that the updates for that workspace have been successfully processed on rails end.
      - This persisted resource version will also help with sending only the latest workspaces changes from agentk to rails for `workspace_updates` message with `partial` update type.
  - `partial`
    - `agentk`
      - Send the latest workspace changes to rails that are not yet persisted in postgres. This persisted resource version will help with sending only the latest workspaces changes from agentk to rails.
    - `rails`
      - Update postgres with the current state and respond with the workspaces to be created/updated/deleted in the kubernetes cluster and their last resource version that rails has persisted in postgres.
      - The workspaces to be created/updated/deleted are calculated by using the filter `desired state updated at >= agent info reported at`.
      - Returning the persisted resource version back to agentk gives it a confirmation that the updates for that workspace have been successfully processed on rails end.

#### Rails API structure

There are multiple types of messages that Agentk can send to Rails. Having a single endpoint in Rails to handle all these different message types with different request/response structure is possible. Agentk, which is written in Go, can possibly handle this dynamic request/response structure with [RPC's `oneof` declaration](https://developers.google.com/protocol-buffers/docs/proto3#oneof). However, it would be easier to keep these message types as separate endpoints in Rails. This would allow better code clarity both on Rails and Agentk side, simpler OpenAPI specification, ease of adding new types of messages and ease of evolution of existing message type's request/response.

#### High level overview of polling architecture

```plantuml
!pragma teoz true

box gitlab monolith #Beige
participant rails order 20
box kas #Bisque
participant "kas" as kas order 40
end box
end box

box Kubernetes cluster #Beige
box agentk #Bisque
participant "agentk remote_development\nmodule" as agentk_rd_mod order 50
end box
participant kubernetes order 60
end box


loop forever
  agentk_rd_mod -> kubernetes: Subscribe to kubernetes changes related to workspace
  activate agentk_rd_mod

  autoactivate on
  agentk_rd_mod -> kas: POST request with\nupdated workspace status/info
  note right
    Any updated workspace status/info from
    kubernetes is pushed with next poll.
  end note
  kas -> rails: proxy POST request from agentk to rails
  return Respond with any new work to achieve\nthe desired state of the workspaces
  return proxy workspace status\nactions to agentk
  autoactivate off

  agentk_rd_mod -> kubernetes: Apply any received workspace status actions to kubernetes
  deactivate agentk_rd_mod
end loop
```

#### Simplified example of applying a user-initiated request for workspace start

```plantuml
!pragma teoz true

actor user order 10
box gitlab monolith #Beige
participant rails order 20
participant postgres order 30
participant gitaly order 35
box kas #Bisque
participant "kas" as kas order 40
end box
end box

box Kubernetes cluster #Beige
box agentk #Bisque
participant "agentk remote_development\nmodule" as agentk_rd_mod order 50
end box
participant kubernetes order 60
end box

autoactivate on

== user-initiated operation ==

group user-initiated request for work
user -> rails: request new workspace\nstart via GraphQL API
rails -> postgres: INSERT **workspace** record\ndesired_state: **started**
return record insert committed
return success response
end

group GA4K polling
group first poll

agentk_rd_mod -> agentk_rd_mod: first poll initiated

agentk_rd_mod -> kas: POST request to rails\nover gRPC connection to kas\n(empty, no workspace status to send)

kas -> rails: proxy POST request from agentk to rails

rails -> gitaly: Retrieve additional non-persisted\nmetadata for work\n(e.g. via Gitaly API)
return metadata

rails -> rails: Internally generate YAML for\nKubernetes resources to create\n(e.g. DevWorkspace config)
return Generated YAML

return send workspace status action\n**desired_state=started**\nto kas in response body\nwith kubernetes resources config payload
return proxy workspace status\nactions to agentk

agentk_rd_mod -> kubernetes: create and start workspace

kubernetes -> kubernetes: kubernetes starts\nworkspace
return workspace started

return publish event for\nworkspace started

agentk_rd_mod -> agentk_rd_mod: Add status change to\nqueue for next poll
return status change queued
return first poll complete
end 'group first poll'

group second poll
agentk_rd_mod -> agentk_rd_mod: second poll initiated

agentk_rd_mod -> kas: POST request to rails\nover gRPC connection to kas\nwith **actual_state=started**\nfrom previous poll
kas -> rails: proxy POST request from agentk to rails
rails -> postgres: UPDATE workspace record\nactual_state: **started**
return record update committed
return success response\nindicating status was successfully updated
return send update indicating\nevent was successfully processed
return second poll complete
end 'group second poll'
end 'group GA4K polling'
rails -> user: Inform user of workspace status change
```


#### Detailed polling architecture

```plantuml
!pragma teoz true

box gitlab monolith #Beige
participant rails order 20
box kas #Bisque
participant "kas" as kas order 40
end box
end box

box Kubernetes cluster #Beige
box agentk #Bisque
participant "agentk remote_development\nmodule" as agentk_rd_mod order 50
end box
participant kubernetes order 60
end box

autoactivate on

group GA4K polling

group kubernetes informer setup [run everytime agent starts/restarts/leader-election;\nruns forever]

agentk_rd_mod -> kubernetes: Subscribe to changes in workspace resources i.e. setup informer
return send informer info about\nworkspace created/updated/deleted

agentk_rd_mod -> agentk_rd_mod: update local information about the  config changes of the\nworkspaces that are created/updated/deleted
deactivate

end 'kubernetes informer setup'

group Prerequisites [run everytime agent starts/restarts/leader-election]

agentk_rd_mod -> agentk_rd_mod: prerequisites poll initiated

agentk_rd_mod -> kas: POST request to rails of type\n"prerequisites"

kas -> rails: proxy POST request from agentk to rails

rails -> rails : Prerequisites Processing
deactivate
return send config to apply for components used\nin each workspace e.g. editor

return proxy work to agentk

agentk_rd_mod -> kubernetes: apply received kubernetes resources
return send info if resources were applied successfully or not

return 'Prerequisites' complete

end 'group Prerequisites'


group Workspace Updates Full [run everytime agent starts/restarts/leader-election;\nruns after "Prerequisites";\nruns every X seconds as configured in full_sync_interval]

agentk_rd_mod -> agentk_rd_mod: full_sync poll initiated

agentk_rd_mod -> agentk_rd_mod: stop active worker if it exists (along with its informer);\ncreate new active worker with fresh informer\ninitiate reconciliation logic for new active worker

agentk_rd_mod -> agentk_rd_mod: gather the information for all workspace as provided by the informer
deactivate

agentk_rd_mod -> kas: POST request to rails with \nmessage type"workspace_updates"\nupdate_type: "full"

kas -> rails: proxy POST request from agentk to rails

rails -> rails : Workspace Updates Processing for "full" update type
deactivate

return send last persisted resource versionof all workspaces\nmanaged by agent and the config to apply for workspaces\nto be created/updated/deleted
return proxy work to agentk

agentk_rd_mod -> agentk_rd_mod: update local information about the last\npersisted resource version of all workspaces
deactivate

agentk_rd_mod -> kubernetes: apply received kubernetes resources
return send info if resources were applied successfully or not

return schedule next full_sync poll

return 'Workspace Updates Full' complete

end 'Workspace Updates Full'


group Workspace Updates Partial [runs every X seconds as configured in partial_sync_interval]

agentk_rd_mod -> agentk_rd_mod: partial_sync poll initiated

agentk_rd_mod -> agentk_rd_mod: initiate reconciliation logic for active worker

agentk_rd_mod -> agentk_rd_mod: generate the information for workspace changes that have\noccurred since the last persisted resource version of that workspace
deactivate

agentk_rd_mod -> kas: POST request to rails with\nmessage type "workspace_updates"\nupdate type "partial"

kas -> rails: proxy POST request from agentk to rails

rails -> rails : Workspace Updates Processing for "partial" update type
deactivate

return send last persisted resource version of all workspaces\nreceived by agent and the config to apply for workspaces\nto be created/updated/deleted
return proxy work to agentk

agentk_rd_mod -> agentk_rd_mod: update local information about the last\npersisted resource version of all workspaces which were sent to rails
deactivate

agentk_rd_mod -> kubernetes: apply received kubernetes resources

return send info if resources were applied successfully or not

return schedule next partial_sync poll

return 'Workspace Updates Partial' complete

end 'Workspace Updates Partial'

end 'group GA4K polling'
```

#### Event driven polling vs full/partial workspace updates

It was initially considered desirable to be able to tell agentk to not wait for the next polling period but instead poll immediately. This would grant the following benefits:

- This would grant the ability to trigger a full sync on demand that would allow on-demand recovery/resetting of module state in agentk
- Apart from making the architecture more event-driven it would also help to increase the interval between polls, thus reducing the load on the infrastructure

However, as the prospective solutions were evaluated, it was concluded that there are very few / rare cases that would merit this capability, especially given the complexity of the viable options. An eventual reconciliation of state would suffice for most cases and it could be simply achieved through full sync that is carried out periodically (perhaps with a longer interval).

[Relevant issue](https://gitlab.com/gitlab-org/gitlab/-/issues/387090) and [conclusion](https://gitlab.com/gitlab-org/remote-development/gitlab-remote-development-docs/-/merge_requests/13#note_1282495106)

### Workspace States

- `Creating` - Initial state of a Workspace; Creation requested by user but hasn't yet been acted on
- `Starting` - In the process of being ready for use
- `Running` - Ready for use
- `Stopping` - In the process of scaling down
- `Stopped` - Persistent storage is still available but workspace has been scaled down
- `Failed` - Kubernetes resources have been applied by agentk but are not ready due to various reasons(e.g. crashing container)
- `Error` - Kubernetes resources failed to get applied by agentk (related - https://gitlab.com/gitlab-org/gitlab/-/issues/397001)
- `Restarting` - User has requested a restart of the workspace but the restart has not yet successfully happened
- `Terminating` - User has requested the termination of the workspace and the action hasn't completed yet
- `Terminated` - Persistent storage has been deleted and the workspace has been scaled down
- `Unknown` - Not able to understand the actual state of the workspace

See [Workspace Updates](./workspace-updates.md) to see different state transitions of workspace, 
the conditions under which it can occur and how rails responds to agentk.

#### Possible 'actual_state' values

The `actual_state` values are determined from the `status` attribute in the Kubernetes Deployment changes
  which `agentk` listens to and sends to Rails.

The following diagram represents the normal flow of the `actual_state` values for a `Workspace` record based on the
`status` values received from `agentk`. The `status` is parsed to derive the `actual_state` of the workspace based on different conditions.

However, any of these states can be skipped if there have been any
transitional `status` updates which were not received from `agentk` for some reason (a quick transition, a
failure to send the event, etc).

```plantuml
[*] --> Creating
Creating : Initial state before\nworkspace creation\nrequest is sent\nto kubernetes
Creating -right-> Starting : status=Starting
Creating -right-> Error : Could not create\nworkspace

Starting : Workspace config is being\napplied to kubernetes
Starting -right-> Running : status=Running
Starting -down-> Failed : status=Failed\n(container crashing)

Running : Workspace is running
Running -down-> Stopping : status=Stopping
Running -down-> Failed : status=Failed\n(container crashing)
Running -down-> Terminated : status=Terminated
Running -right-> Error : Could not\nstop/terminate\nworkspace

Stopping : Workspace is stopping
Stopping -down-> Stopped : status=Stopped
Stopping -left-> Failed : status=Failed\n(could not\nunmount volume\nand stop workspace)

Stopped : Workspace is Stopped\nby user request
Stopped -left-> Failed : status=Failed\n(could not\nunmount volume\nterminate workspace)
Stopped -right-> Error : Could not\nstart/terminate\nworkspace
Stopped -down-> Terminated : status=Terminated
Stopped -up-> Starting : status=Starting

Terminated: Workspace has been deleted

Failed: Workspace is not ready due to\nvarious reasons(e.g. crashing container)
Failed -up-> Starting : status=Starting\n(container\nnot crashing)
Failed -right-> Stopped : status=Stopped
Failed -down-> Terminated : status=Terminated
Failed -down-> Error : Could not\nstop/terminate\nworkspace

Error: Kubernetes resources failed to get applied
Error -up-> Terminated : status=Terminated

Unknown: Unable to understand the actual state of the workspace
```

#### Possible 'desired_state' values

The `desired_state` values are determined from the user's request to Rails, and are sent to `agentk` by Rails.

`desired_state` is a subset of the `actual_state` with only `Running`, `Stopped`, `Terminated` and `Restarting` values.
The state reconciliation logic in Rails will
continually attempt to transition the `actual_state` to the `desired_state` value, unless the workspace is in an unrecoverable state.

There is also an additional supported state of `Restarting` which is only valid for `desired_state`.
This value is not a valid value for `actual_state`. It is required in order for Rails to
initiate a restart of a started workspace. It will only persist until a `status` of `Stopped` is received
from `agentk`, indicating that the restart request was successful and in progress or completed.
At this point, the `desired_state` will be automatically changed to `Running` to trigger the workspace to restart again.
If there is a failure to restart the workspace, and a `Stopped` status is never received, the
`desired_state` will remain `Restarting` until a new `desired_state` is specified.

```plantuml
[*] --> Running
Running : Workspace is running
Running -down-> Stopped : status=Stopped
Running -left-> Terminated : status=Terminated

Stopped : Workspace is Stopped\nby user request
Stopped -up-> Running : status=Running
Stopped -down-> Terminated : status=Terminated

Terminated: Workspace has been deleted

Restarting : User has requested a workspace restart.\n**desired_state** will automatically change\nto **'Running'** if actual state\nof **'Stopped'** is received.
Restarting -left-> Running : status=Running
```

### 'agentk' calculation of workspace changes for workspace updates

TODO: Update this section based on outcome of https://gitlab.com/gitlab-org/gitlab/-/issues/387010

When `agentk` listens to kubernetes changes for each workspace, it will keep a record of the Deployment resource version for the most recent change for that workspace which has been successfully sent to rails and processed.

This is accomplished by sending the workspace resource version to rails in the POST request, and Rails will echo them
back if the `actual_state` represented by the change were persisted successfully.

### Rails 'actual_state' event timestamps

TODO: Figure out if this is required if we are doing full resync based
on how https://gitlab.com/gitlab-org/gitlab/-/issues/397003 is implemented

Rails will persist a timestamp of the most recent update to the `actual_state` field for each workspace.

This allows Rails to "retry" sending the `desired_state` to `agentk` if a workspace fails to be updated
to the `desired_state` in a timely manner.

This will allow for recovery if the initial update attempt fails, for example due to a network error somewhere
in the communication chain between `rails`, `kas`, `agentk`, or kubernetes.

TODO: Add more details and a diagram of this.
